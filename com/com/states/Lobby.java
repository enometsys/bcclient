package com.states;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.state.StateBasedGame;

import com.Match;
import com.Match.ListMatch;
import com.packet.tcp.TCPPacketBuilder;
import com.packet.tcp.response.MatchDetails;
import com.states.base.MatchState;
import com.states.base.NiftyUIController.LobbyInterf;
import com.utils.NiftyDetails;
import com.utils.Util;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyDelayedMethodInvoke;
import de.lessvoid.nifty.controls.ListBox;

public final class Lobby extends MatchState implements LobbyInterf{
	//private static final String TAG = MatchState.class.getSimpleName();
	
	private Map<String, Match.ListMatch> 		matchList;
	private ListBox<String> 						lstRoomList;
	private ListMatch								selectedMatch;
	
	public Lobby() {
		super(Util.STATE_LOBBY);
		matchList = new HashMap<String, Match.ListMatch>();
	}
	
	@Override
	protected LobbyInterf getLobbyInterf() {
		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void additionalElementsAfterChat(Nifty nifty, StateBasedGame sbg) {
		lstRoomList = nifty.getScreen(NiftyDetails.K.SCRN_START)
				.findNiftyControl(NiftyDetails.K.LST_ROOM_LIST, ListBox.class);
	}
	
	

	@Override
	protected void enteringState() {
		refreshHit();
	}

	@Override
	public void hostMatch(String matchName) {
		sendTCPPacket(TCPPacketBuilder.getHostMatch(matchName));
	}

	@Override
	public void joinSelectedMatch() {
		if (selectedMatch != null)
			sendTCPPacket(TCPPacketBuilder.getJoinMatch(selectedMatch.getMatchId()));
	}

	@Override
	public String getSelectedMatch() {
		String temp = lstRoomList.getFocusItem();
		
		if (temp != null){
			if (matchList.containsKey(temp)){
				selectedMatch = matchList.get(temp);
				return selectedMatch.getMatchName();
			}
		}

		return null;
	}

	@Override
	public void refreshHit() {
		lstRoomList.clear();
		matchList.clear();
		selectedMatch = null;
		sendTCPPacket(TCPPacketBuilder.getRefreshRooms());
	}
	
	private void redrawListMatches(){
		getNifty().delayedMethodInvoke(new NiftyDelayedMethodInvoke(){
			@Override
			public void performInvoke(Object... invokeParametersParam) {
				lstRoomList.clear();
				lstRoomList.addAllItems(matchList.keySet());
			}
		}, (Object[]) null);
	}
	
	protected final void receiveMatchRooms(MatchDetails.List matchDetails){
		ListMatch match = new ListMatch(matchDetails);
		
		if (matchList.containsKey(match.toString())){
			if (!matchDetails.isDisplay()){
				matchList.remove(match.toString());
			}
		}else{
			matchList.put(match.toString(), match);
		}
		
		redrawListMatches();
	}

}
