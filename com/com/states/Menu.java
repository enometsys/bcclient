package com.states;

import java.util.Random;

import com.Main;
import com.connection.TCPConnectionHandler.FailurReceiver;
import com.packet.tcp.TCPPacket;
import com.packet.tcp.TCPPacketBuilder;
import com.packet.tcp.response.JoinServerResponse;
import com.packet.tcp.response.Response;
import com.states.base.NiftyUIController;
import com.states.base.NiftyUIController.MenuInterf;
import com.utils.Util;

import de.lessvoid.nifty.NiftyDelayedMethodInvoke;

public final class Menu extends NiftyUIController implements MenuInterf, FailurReceiver{
	private static final String TAG = Menu.class.getSimpleName();
	private final TCPConnector tcpConnector;
	
	public Menu(TCPConnector tcpConnector) {
		super(Util.STATE_MENU);
		this.tcpConnector = tcpConnector;
	}
	
	@Override
	protected MenuInterf getMenuInterf() {
		return this;
	}

	@Override
	public void receiveTCPPacket(TCPPacket inPacket) {
		switch(inPacket.getTcpPacketType()){
			case RESPONSE:
				Response resPacket = (Response) inPacket;
				switch( resPacket.getReqType() ){
					case JOIN_SERVER:
						
						if (resPacket.isError()){
							//show error message
							showError(resPacket.getMessage());
						}else{
							JoinServerResponse joinRes = (JoinServerResponse) inPacket;
							
							//login as accepted playerId and name
							loginAs(joinRes.getPlayerId(), joinRes.getPlayerName());
						}
						
						break;
					default:
						Main.log.w(TAG, "Unnecessary Packet " + inPacket.toString() + " Received! Well, it shouldn't!!!!");
						break;
				}
				break;
			case CHAT:
				Main.log.w(TAG, "Chat Packet " + inPacket.toString() + " Received! Well, it shouldn't!!!!");
				break;
			case REQUEST:
				Main.log.w(TAG, "Request Packet " + inPacket.toString() + " Received! Well, it shouldn't!!!!");
				break;
			default:
				break;
				
		}
	}
	
	protected void connectedToServer(){
		if (Main.autoLogin){
			Random r = new Random();
			sendPlayerNameRequest("Metsys" + (r.nextInt(999) + 1));
		}else{
			getNifty().delayedMethodInvoke(new NiftyDelayedMethodInvoke(){
				@Override
				public void performInvoke(Object... invokeParametersParam) {
					askForInput("Connected to server! Enter name:");
				}
			}, (Object[]) null);
		}
	}
	
	@Override
	public void failedToConnect(String reason){
		showError("Failed to connect! " + reason);
	}
	
	

	@Override
	protected void enteringState() {
		
	}

	@Override
	public void connectToServer(String ipAddress) {
		tcpConnector.connectToServer(ipAddress, this);
	}

	@Override
	public void sendPlayerNameRequest(String playerName) {
		sendTCPPacket(TCPPacketBuilder.getJoinServer(playerName));
	}

	public static interface TCPConnector{
		void connectToServer(String ipAddress, FailurReceiver failureReceiver);
	}
}
