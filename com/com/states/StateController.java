package com.states;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;

import com.Main;

import de.lessvoid.nifty.slick2d.NiftyStateBasedGame;

public class StateController extends NiftyStateBasedGame{
	//private static final String TAG = StateController.class.getSimpleName();
	
	private final Menu 					menu;
	private final Lobby 				lobby;
	private final Room 					room;
	private final Game 					game;

	public StateController(String name, Menu menu, Lobby lobby, Room room, Game game) {
		super(name);
		this.menu = menu;
		this.lobby = lobby;
		this.room = room;
		this.game = game;
	}

	@Override
	public void initStatesList(GameContainer arg0) throws SlickException {
		if (Main.test){
			addState(game);
			addState(menu);
			//addState(lobby);
			//addState(room);
		}else{
			addState(menu);
			addState(lobby);
			//addState(room);
			//addState(game);
		}
	}
}
