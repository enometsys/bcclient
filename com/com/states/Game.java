package com.states;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.newdawn.slick.Input;

import com.Main;
import com.connection.UDPConnectionHandler;
import com.connection.UDPConnectionHandler.ConnectionInterface;
import com.connection.UDPConnectionHandler.FailurReceiver;
import com.objects.GameObject.State;
import com.objects.utils.ObjectDetails;
import com.objects.utils.ObjectDetails.K;
import com.packet.udp.Action;
import com.packet.udp.UDPPacket;
import com.packet.udp.UDPPacketBuilder;
import com.states.game.GameState;

public final class Game extends GameState implements ConnectionInterface, FailurReceiver{
	private static final String TAG = Game.class.getSimpleName();
	
	private UDPConnectionHandler udpConnection;
	private final UDPConnector udpConnector;
	
	public Game(UDPConnector udpConnector) {
		super();
		this.udpConnector = udpConnector;
	}
	
	@Override
	protected void enteringState() {
		if (Main.test){
			loginAs(0, "TestPlayer0");
			createMatch(0, "Test Match", 0, 0);
			updatePlayers(0, "TestPlayer 0", 4, false, true);
			updatePlayers(1, "TestPlayer 1", 0, false, true);
			try {
				startMatch(InetAddress.getByName("226.1.1.1"), 4020, InetAddress.getLocalHost(),3020);
			} catch (UnknownHostException e) {
				System.exit(1);
				e.printStackTrace();
			}
		}
	}

	
	float locX = 600, locY = 900;
	@Override
	public void keyPressed(int key, char c) {
		if (Main.test){
			
			switch(key){
				case Input.KEY_1: receiveUDPPacket(UDPPacketBuilder
						.getActionPacket(1, 0, K.unit_archer, 800, ObjectDetails.getIntState(State.IDLE_75), 600, 900));
					break;
				case Input.KEY_2: receiveUDPPacket(UDPPacketBuilder
						.getActionPacket(1, 0, K.unit_archer, 800, ObjectDetails.getIntState(State.IDLE_75), locX+=10, locY+=10));
					break;
				case Input.KEY_3: receiveUDPPacket(UDPPacketBuilder
						.getActionPacket(1, 2, K.unit_dragon, 800, ObjectDetails.getIntState(State.IDLE_75), 600, 940));
					break;
				case Input.KEY_4: receiveUDPPacket(UDPPacketBuilder
						.getActionPacket(1, 3, K.unit_hogrider, 800, ObjectDetails.getIntState(State.IDLE_75), 600, 950));
					break;
				case Input.KEY_0: receiveUDPPacket(UDPPacketBuilder
						.getActionPacket(1, 4, K.build_wall, 800, ObjectDetails.getIntState(State.IDLE_75), 600, 960));
					break;
			}
			
		}else super.keyPressed(key, c);
	}

	@Override
	public void failedToStartUDPConnection(String reason) {
		togglePause(true);
		showError(reason);
	}
	
	@Override
	public void leaveMatch() {
		if (udpConnection != null)
			udpConnection.disconnect();
	}
	
	@Override
	protected void mapInitialized() {
		sendUDPPacket(UDPPacketBuilder.getPlayerStarted(getPlayerInfo().getPlayerId()));
	}

	@Override
	public void UDPConnected(UDPConnectionHandler udpConnection) {
		this.udpConnection = udpConnection;
		this.initializeMap(getJoinedMatch().getMapId(), 20, 20);
	}

	@Override
	public void sendUDPPacket(UDPPacket outPacket) {
		if (udpConnection != null)
			udpConnection.sendPacket(outPacket);
	}

	@Override
	public void receiveUDPPacket(UDPPacket inPacket) {
		switch(inPacket.getUdpPacketType()){
			case UDPPacket.UDPPacketType.GAME_OVER:
				Main.log.r(TAG, inPacket);
				gameOver(inPacket.getPlayerId());
				break;
			case UDPPacket.UDPPacketType.STARTED:
				if (getPlayerInfo().isMe(inPacket.getPlayerId())) return;
				
				Main.log.r(TAG, inPacket);
				addToPlayerInitializationQueue(inPacket.getPlayerId());
				break;
			case UDPPacket.UDPPacketType.ACTION:
				
				if (getPlayerInfo().isMe(inPacket.getPlayerId())) return;
				
				Main.log.r(TAG, inPacket);
				addToPlayerInitializationQueue(inPacket.getPlayerId());
				receiveUpdate((Action)inPacket);
				break;
			default:
				break;
		}
	}

	@Override
	public void updateObject(UDPPacket dPckt) {
		sendUDPPacket(dPckt);
	}

	@Override
	public void UDPdisconnected() {
		udpConnection = null;
	}

	@Override
	protected void disconnectedFromServer() {
		super.disconnectedFromServer();
		leaveMatch();
	}


	@Override
	protected void startUDPConnection(InetAddress groupAddress, int groupPort, InetAddress serverAddress,
			int serverPort) {
		udpConnector.startUDPConnection(groupAddress, groupPort, serverAddress, serverPort);
	}
	
	public static interface UDPConnector{
		void startUDPConnection(InetAddress groupAddress, int groupPort, InetAddress serverAddress,
				int serverPort);
	}
}
