package com.states.base;

import org.newdawn.slick.Input;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.packet.tcp.TCPPacket;
import com.packet.tcp.TCPPacketBuilder;
import com.packet.tcp.TCPPacket.Scope;
import com.packet.tcp.chat.Chat;
import com.packet.tcp.response.Response;
import com.states.base.NiftyUIController.ChatInterf;
import com.utils.NiftyDetails;
import com.utils.Util;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyDelayedMethodInvoke;
import de.lessvoid.nifty.controls.ListBox;
import de.lessvoid.nifty.controls.TextField;

public abstract class ChatState extends NiftyUIController implements ChatInterf{
	private static final String TAG = ChatState.class.getSimpleName();
	
	private ListBox<String> 			lstChatBox;
	private TextField 					txtChatInput;
	
	public ChatState(int stateId) {
		super(stateId);
	}
	
	@Override
	public final ChatInterf getChatInterf() {
		return this;
	}

	@Override
	protected final void additionalElements(Nifty nifty, StateBasedGame sbg) {
		txtChatInput 	= getChatInputField(nifty, sbg);
		lstChatBox 	= getChatListBox(nifty, sbg);
		
		additionalElementsAfterChat(nifty, sbg);
	}
	
	protected final TextField getChatInputField(Nifty nifty, StateBasedGame sbg) {
		
		if (getID() == Util.STATE_GAME)
			return getPopup(NiftyDetails.K.POP_CHAT).findNiftyControl(NiftyDetails.K.TXT_CHAT_INPUT, TextField.class);
		
		return nifty.getScreen(NiftyDetails.K.SCRN_START)
				.findNiftyControl(NiftyDetails.K.TXT_CHAT_INPUT, TextField.class);
	}

	@SuppressWarnings("unchecked")
	protected final ListBox<String> getChatListBox(Nifty nifty, StateBasedGame sbg) {
		if (getID() == Util.STATE_GAME)
			return getPopup(NiftyDetails.K.POP_CHAT).findNiftyControl(NiftyDetails.K.LST_CHAT_BOX, ListBox.class);
		
		return nifty.getScreen(NiftyDetails.K.SCRN_START)
				.findNiftyControl(NiftyDetails.K.LST_CHAT_BOX, ListBox.class);
	}
	
	protected void additionalElementsAfterChat(Nifty nifty, StateBasedGame sbg){
		
	}

	protected abstract void receiveResponsePackets(Response inPacket);
	
	@Override
	public final void receiveTCPPacket(TCPPacket inPacket) {
		switch(inPacket.getTcpPacketType()){
			case CHAT:
				Chat cht = (Chat) inPacket;
				if (getID() == Util.STATE_LOBBY){
					if (inPacket.getScope() == Scope.GLOBAL)
						displayChat(cht.getFrom(), cht.getMessage());
				}else{
					if (inPacket.getScope() == Scope.ROOM)
						displayChat(cht.getFrom(), cht.getMessage());
				}
				break;
			case REQUEST:
				Main.log.w(TAG, "Request Packet " + inPacket.toString() + " Received! Well, it shouldn't!!!!");
				break;
			case RESPONSE:
				receiveResponsePackets( (Response) inPacket );
				break;
			default:
				break;
		}
	}
	
	
	/**
	 * ======================================= CHAT FUNCTIONS ==========================================
	 */
	@Override
	public void keyPressed(int key, char c) {
		switch(key){
			case Input.KEY_RETURN:
				if(!txtChatInput.hasFocus()) break;
				sendChat();
				return;
			default:
				break;
		}
		
		super.keyPressed(key, c);
	}
	
	
	private void displayChat(String from, String message) {
		getNifty().delayedMethodInvoke(new NiftyDelayedMethodInvoke(){
			@Override
			public void performInvoke(Object... invokeParametersParam) {
				lstChatBox.addItem(from + ": " + message);
			}
		}, (Object[]) null);
	}
	

	@Override
	public void sendChat() {
		String message = txtChatInput.getDisplayedText().trim();		
		if (message.isEmpty()){
			return;
		}
		
		txtChatInput.setText("");
		
		sendTCPPacket(TCPPacketBuilder.getChat(((getID() == Util.STATE_LOBBY)?	Scope.GLOBAL:Scope.ROOM), 
				getPlayerInfo().getPlayerName(), message));
		
		displayChat("Me", message);
	}
	
}
