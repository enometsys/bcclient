package com.states.base;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.utils.NiftyDetails;
import com.utils.Util;
import com.utils.NiftyDetails.K;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

public abstract class NiftyUIController extends NiftyUI{
	private ChatInterf 		chatInterf;
	private MenuInterf		menuInterf;
	private LobbyInterf		lobbyInterf;
	private RoomInterf		roomInterf;
	private GameInterf		gameInterf;	
	
	private	GUIController 	guiController;
	
	public NiftyUIController(int stateId) {
		super(stateId);
		this.chatInterf = getChatInterf();
		this.menuInterf = getMenuInterf();
		this.lobbyInterf = getLobbyInterf();
		this.roomInterf = getRoomInterf();
		this.gameInterf = getGameInterf();
		this.guiController = new GUIController();
	}
	
	protected ChatInterf getChatInterf(){
		return null;
	}
	
	protected MenuInterf getMenuInterf(){
		return null;
	}
	
	protected LobbyInterf getLobbyInterf(){
		return null;
	}
	
	protected RoomInterf getRoomInterf(){
		return null;
	}
	
	protected GameInterf getGameInterf(){
		return null;
	}
	
	protected ScreenController getGUIController() {
		return guiController;
	}
	
	/**
	 * ============================ INTERCEPT METHODS NOT FOR OTHER CLASSES ================================
	 */
	@Override
	protected void additionalElements(Nifty nifty, StateBasedGame sbg) {}
	
	@Override
	protected void renderGameState(GameContainer gc, StateBasedGame sbg, Graphics g) {}

	@Override
	protected void updateGameState(GameContainer gc, StateBasedGame sbg, int delta) {}
	
	@Override
	protected boolean hasJoinedMatch() {return false;}

	@Override
	protected boolean joinedMatchStarted() {return false;}

	@Override
	protected void unregisterJoinedMatch() {}
	
	/**
	 * ============================ ACTION LISTENER INTERFACES ================================
	 */
	public final class GUIController implements ScreenController{
		@Override
		public void bind(Nifty arg0, Screen arg1) {}

		@Override
		public void onEndScreen() {}

		@Override
		public void onStartScreen() {}
		
		public void cancel(String popupName){
			togglePopup(popupName, false);
		}

		public void back(){
			backQuery();
		}
		
		public void exit(){
			showChoice(K.QRY_EXIT, "Exit Game?");
		}
		
		public void yes(){
			switch(getMark()){
				case K.QRY_BACK:
					if (roomInterf != null)
						roomInterf.leaveMatch();
					
					if (gameInterf != null)
						gameInterf.leaveMatch();
					
					prevState();
					break;
				
				case K.QRY_EXIT:
					if (gameInterf != null)
						gameInterf.leaveMatch();
					
					exitGame();
					break;
					
				case K.QRY_JOIN:
					if (lobbyInterf != null){
						lobbyInterf.joinSelectedMatch();
					}
					
					break;
					
				case K.QRY_BACK_MENU:
					backToMenu();
					break;
			}
	
			closeChoice();
		}
		
		public void enter(){
			
			if (getInput().isEmpty()){
				closeInput();
				showError("Input cannot be empty!");
				return;
			}
			
			switch(getID()){
				case Util.STATE_MENU:
					if (menuInterf != null){
						if (isConnectedToServer()){
							menuInterf.sendPlayerNameRequest(getInput());
						}else{
							menuInterf.connectToServer(getInput());
						}
					}
					break;
				case Util.STATE_LOBBY:
					if (lobbyInterf != null){
						lobbyInterf.hostMatch(getInput());
					}
			}
			
			closeInput();
		}
		
		public void start(){
			if (getID() != Util.STATE_MENU) return;
			
			if (isConnectedToServer()){
				if (getPlayerInfo() == null)
					askForInput("Connected to server! Enter name:");
				else 
					nextState();
			}else{
				askForInput("Enter server ip address:");
			}
		}
		
		public void hostMatch(){
			Main.log.d("GUI Controller", "hostMatch hit! by " + Util.getStateName(getID()));
			
			if (getID() != Util.STATE_LOBBY) return;
			
			if (!isConnectedToServer()){
				showError("Disconnected to server!");
				return;
			}
			
			Main.log.d("GUI Controller", "Asking for input...");
			askForInput("Enter matchname:");
		}
		
		public void joinMatch(){
			Main.log.d("GUI Controller", "joinMatch hit! by " + Util.getStateName(getID()));
			
			if (getID() != Util.STATE_LOBBY) return;
			
			if (!isConnectedToServer()){
				showError("Disconnected to server!");
				return;
			}
			
			String selectedMatch = lobbyInterf.getSelectedMatch();
			
			if (selectedMatch != null){
				showChoice(K.QRY_JOIN, "Join match " + selectedMatch + "?");
			}else{
				showError("Select a match to join!");
			}
		}
		
		public void refresh(){
			Main.log.d("GUI Controller", "refresh hit! by " + Util.getStateName(getID()));
			
			if (getID() != Util.STATE_LOBBY) return;
			
			if (!isConnectedToServer()){
				showError("Disconnected to server!");
				return;
			}
			
			if (lobbyInterf != null)
				lobbyInterf.refreshHit();
		}
		
		public void sendChat(){
			if (getID() == Util.STATE_MENU) return;
			
			if (chatInterf != null)
				chatInterf.sendChat();
		}
		
		public void readyStart(){
			if (getID() != Util.STATE_ROOM) return;
			
			if (roomInterf != null)
				roomInterf.readyStartHit();
		}
		
		public void mapChange(String direction){
			if (getID() != Util.STATE_ROOM) return;
			
			if (roomInterf != null){
				switch(direction){
					case NiftyDetails.K.MAP_CHANGE_NEXT:
						roomInterf.changeMapNext();
						break;
					case NiftyDetails.K.MAP_CHANGE_PREV:
						roomInterf.changeMapPrev();
						break;
				}
			}
		}
		
		public void options(){
			if (getID() != Util.STATE_GAME) return;
			
			if (gameInterf != null)
				gameInterf.showOptions();;
		}
		
	}
	
	public static interface ChatInterf{
		void sendChat();
	}
	
	public static interface GameInterf{
		void showOptions();
		void leaveMatch();
	}
	
	public static interface LobbyInterf{
		void hostMatch(String matchName);
		void joinSelectedMatch();
		String getSelectedMatch();
		void refreshHit();
	}

	public static interface MenuInterf{
		void connectToServer(String ipAddress);
		void sendPlayerNameRequest(String playerName);
	}
	
	public static interface RoomInterf{
		void readyStartHit();
		void leaveMatch();
		void changeMapPrev();
		void changeMapNext();
	}	
}
