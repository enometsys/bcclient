package com.states.base;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.utils.NiftyDetails;
import com.utils.Util;
import com.utils.NiftyDetails.K;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.controls.TextField;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.screen.ScreenController;

public abstract class NiftyUI extends BaseState{
	private static final String TAG = NiftyUI.class.getSimpleName();
	
	public NiftyUI(int stateId) {
		super(stateId);
	}
	
	@Override
	protected final void EnterOrLeaveState(boolean enter) {
		if (enter) enteringState();
		else leavingState();
		closeAllPopups();
		clearInputFields();
	}
	
	protected void enteringState(){
		
	}
	
	protected void leavingState(){
		
	}
	
	@Override
	protected void disconnectedFromServer() {
		closeAllPopups();
		showChoice(K.QRY_BACK_MENU,"Disconnected from Server! Back to Main menu?");
	}
	
	/**
	 * ========================== NIFTY UI INIT and CONTROL ===================================
	 */
	@Override
	protected final void prepareNifty(Nifty nifty, StateBasedGame sbg) {
		Main.log.d(TAG, "Preparing nifty for " + Util.getStateName(getID()));
		
		try {
			nifty.validateXml(NiftyDetails.getNiftyUIPath(getID()));
			nifty.fromXml	(NiftyDetails.getNiftyUIPath(getID()), 
							NiftyDetails.getStartScreen(getID()), 
							getGUIController(), getGUIController());
			
			addPopups(nifty, sbg);
			
			ArrayList<String> additionalPopups = new ArrayList<String>();
			additionalPopups = additionalPopups(additionalPopups);
			
			if (additionalPopups != null){
				for(String key:additionalPopups){
					POPUPS.put(key, nifty.createPopup(key));
				}
			}
			
			additionalElements(nifty, sbg);
			
		} catch (Exception e) {
			Main.log.e(TAG, "Error preparing Nifty UI" + e.getMessage());
			e.printStackTrace();
		}
	}
	
	protected ArrayList<String> additionalPopups(ArrayList<String> additionalPopups){
		return additionalPopups;
	}
	
	private void addPopups(Nifty nifty, StateBasedGame sbg){
		if (!POPUPS.isEmpty()) return;		
		try {
			nifty.validateXml(K.PATH_POPUPS);
			nifty.addXml(NiftyDetails.K.PATH_POPUPS);
			
			POPUPS.put(K.POP_PROMPT, nifty.createPopup(K.POP_PROMPT));
			POPUPS.put(K.POP_WAIT, nifty.createPopup(K.POP_WAIT));
			POPUPS.put(K.POP_CHOICE, nifty.createPopup(K.POP_CHOICE));
			POPUPS.put(K.POP_INPUT, nifty.createPopup(K.POP_INPUT));
			
		} catch (Exception e) {
			Main.log.e(TAG,"Error Preparing popup: " + e.getMessage());
			e.printStackTrace();
		}
	}
	
	protected final void gotoScreen(String screenId){
		Nifty nifty = getNifty();
		
		if (nifty.getAllScreensName().contains(screenId)){
			nifty.gotoScreen(screenId);
		}else{
			Main.log.w(TAG, "Screen " + screenId + " doesn't exist on " + Util.getStateName(getID()));
		}
	}
	
	protected abstract void additionalElements(Nifty nifty, StateBasedGame sbg);
	protected abstract ScreenController getGUIController();
	
	/**
	 * ================================ POP-UPS CONTROL ========================================
	 */
	private final Map<String, Element> POPUPS	= new HashMap<String, Element>();
	
	protected final Element getPopup(String popupName){
		if (POPUPS.containsKey(popupName)){
			return POPUPS.get(popupName);
		}
		
		return null;
	}
	
	private boolean popupIsOpen(String popupName){
		if (POPUPS.containsKey(popupName)){
			return getNifty().getCurrentScreen().isActivePopup(POPUPS.get(popupName));
		}
		
		Main.log.e(TAG, "Popup " + popupName + " Doesn't exist on " + Util.getStateName(getID()));
		return false;
	}
	
	protected final void togglePopup(String popupName){
		if (POPUPS.containsKey(popupName)){
			togglePopup(popupName, !popupIsOpen(popupName));
		}else{
			Main.log.e(TAG, "Popup " + popupName + " Doesn't exist on " + Util.getStateName(getID()));
		}
	}
	
	protected final void togglePopup(String popupName, Boolean toggle){
		if (POPUPS.containsKey(popupName)){
			Nifty nifty = getNifty();
			Element popup = POPUPS.get(popupName);
			
			if (toggle){ 	// ---------------   open a pop-up -----------------------
				if (!popupIsOpen(popupName))	// pop-up is closed
					nifty.showPopup(nifty.getCurrentScreen(), popup.getId(), null);
			}else{			// --------------   close a pop-up -----------------------
				if (popupIsOpen(popupName))		// pop-up is open
					nifty.closePopup(popup.getId());
			}
		}else{
			Main.log.e(TAG, "Popup " + popupName + " Doesn't exist on " + Util.getStateName(getID()));
		}
	}
	
	private void closeAllPopups(){
		for(String key: POPUPS.keySet()){
			togglePopup(key, false);
		}
	}
	
	private void clearInputFields(){
		POPUPS.get(K.POP_PROMPT).findNiftyControl(K.POP_LBL_PROMPT, Label.class).setText("");
		POPUPS.get(K.POP_WAIT).findNiftyControl(K.POP_LBL_WAIT, Label.class).setText("");
		POPUPS.get(K.POP_INPUT).findNiftyControl(K.POP_LBL_INPUT, Label.class).setText("");
		POPUPS.get(K.POP_INPUT).findNiftyControl(K.POP_TXT_INPUT, TextField.class).setText("");
		POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_CHOICE, Label.class).setText("");
	}
	
	protected final void showPrompt(String promptMessage){
		closeAllPopups();
		POPUPS.get(K.POP_PROMPT).findNiftyControl(K.POP_LBL_PROMPT, Label.class).setText(promptMessage);
		togglePopup(K.POP_PROMPT, true);
	}
	
	protected final void showError(String errorMessage){
		showPrompt(errorMessage);
	}
	
	protected final void toggleWaiting(boolean toggle, String waitMessage){
		if (toggle){
			closeAllPopups();
			Main.log.d(TAG, "Popup Waiting: " + waitMessage);
			//POPUPS.get(K.POP_WAIT).findNiftyControl(K.POP_LBL_WAIT, Label.class).setText(waitMessage);
			//togglePopup(K.POP_WAIT, true);
		}else{
			Main.log.d(TAG, "Popup Waiting closed.");
			//togglePopup(K.POP_WAIT, false);
		}
	}
	
	protected final void askForInput(String inputMessage){
		closeAllPopups();
		POPUPS.get(K.POP_INPUT).findNiftyControl(K.POP_LBL_INPUT, Label.class).setText(inputMessage);
		POPUPS.get(K.POP_INPUT).findNiftyControl(K.POP_TXT_INPUT, TextField.class).setText("");
		togglePopup(K.POP_INPUT, true);
	}
	
	protected final String getInput(){
		return POPUPS.get(K.POP_INPUT).findNiftyControl(K.POP_TXT_INPUT, TextField.class).getDisplayedText().trim();
	}
	
	protected final void closeInput(){
		togglePopup(K.POP_INPUT, false);
	}
	
	protected final void backQuery(){
		switch(getID()){
			case Util.STATE_LOBBY:
				if (isConnectedToServer())
					showChoice(K.QRY_BACK, "Disconnect from server?");
				else
					showChoice(K.QRY_BACK,"Back to main menu?");
				return;
			case Util.STATE_ROOM:
			case Util.STATE_GAME:
				showChoice(K.QRY_BACK, "Back to lobby?");
				return;
			default:
				break;
		}
		
		Main.log.e(TAG, Util.getStateName(getID()) + " cannot go back.");
	}
	
	protected final void showChoice(String mark, String choiceMessage){
		POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_MARK, Label.class).setText(mark);
		POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_CHOICE, Label.class).setText(choiceMessage);
		togglePopup(K.POP_CHOICE, true);
	}
	
	protected final void closeChoice(){
		togglePopup(K.POP_CHOICE, false);
		POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_MARK, Label.class).setText("");
	}
	
	protected final String getMark(){
		return POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_MARK, Label.class).getText().trim();
	}
	
	protected final void backToMenuQuery(String backMessage){
		POPUPS.get(K.POP_CHOICE).findNiftyControl(K.POP_LBL_CHOICE, Label.class).setText(backMessage);
		togglePopup(K.POP_CHOICE, true);
	}
}
