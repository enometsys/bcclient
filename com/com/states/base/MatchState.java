package com.states.base;

import java.net.InetAddress;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import com.JoinedMatch;
import com.Main;
import com.objects.utils.BarracksDetails;
import com.packet.tcp.response.MatchDetails;
import com.packet.tcp.response.PlayerDetails;
import com.packet.tcp.response.Response;
import com.utils.Util;

public abstract class MatchState extends ChatState{
	private static final String TAG = MatchState.class.getSimpleName();
	
	private static JoinedMatch joinedMatch;
	
	public static boolean hasMatch(){
		return joinedMatch != null;
	}
	
	public static boolean hasStarted(){
		if (hasMatch())
			return joinedMatch.isStarted();
			
		return false;
	}
	
	public static JoinedMatch getSJoinedMatch(){
		return joinedMatch;
	}
	
	public MatchState(int stateId) {
		super(stateId);
	}

	@Override
	protected final boolean hasJoinedMatch() {
		return hasMatch();
	}
	
	protected final JoinedMatch getJoinedMatch(){
		return joinedMatch;
	}

	@Override
	protected final boolean joinedMatchStarted() {
		return hasStarted();
	}

	@Override
	protected final void unregisterJoinedMatch() {
		joinedMatch = null;
		getPlayerInfo().leaveMatch();
	}

	protected final void createMatch(int matchId, String matchName, int mapId, int hostId){
		Main.log.d(TAG, "Creating match on " + Util.getStateName(getID()));
		
		joinedMatch = new JoinedMatch(matchId, matchName, mapId, hostId);
		getPlayerInfo().joinMatch(joinedMatch);
		nextState();
	}
	
	protected final void startMatch(InetAddress groupAddress, int groupPort, InetAddress serverAddress, int serverPort){
		if (!hasJoinedMatch()){
			Main.log.e(TAG, "Cannot start null match.");
			return;
		}
		
		if (joinedMatch.isStarted()){
			Main.log.e(TAG, "Cannot start a started match.");
			return;
		}
		
		Main.log.d(TAG, "Starting Match on " + Util.getStateName(getID()));
		
		BarracksDetails.resetCounts();
		
		joinedMatch.startMatch();
		nextState();
		startUDPConnection(groupAddress, groupPort, serverAddress, serverPort);
	}
	
	protected void startUDPConnection(InetAddress groupAddress, int groupPort, InetAddress serverAddress, int serverPort){
		
	}

	protected final void updateMatch(int matchId, String matchName, int mapId, int hostId){
		if (!hasJoinedMatch()){
			createMatch(matchId, matchName, mapId, hostId);
			return;
		}
		
		Main.log.d(TAG, "Updating match on " + Util.getStateName(getID()));
		
		if (!joinedMatch.isSame(matchId, matchName)){
			Main.log.w(TAG, "Update is different from current match");
			return;
		}
		
		if (joinedMatch.updated(mapId, hostId))
			matchInfoUpdated();
	}
	
	protected final void updatePlayers(int playerId, String playerName, int mapLocation, boolean left, boolean ready){
		if (!hasJoinedMatch()){
			Main.log.e(TAG, "Cannot update players of null match");
			return;
		}
		
		Main.log.d(TAG, "Updating players on " + Util.getStateName(getID()));
		
		if(joinedMatch.playerUpdated(playerId, playerName, mapLocation, left, ready))
		
		if (left) playerLeft(playerName);
			
		playersUpdated();
	}
	
	@Override
	protected void receiveResponsePackets(Response inPacket) {
		switch(inPacket.getReqType()){
			case HOST_MATCH:
			case JOIN_MATCH:
				if (inPacket.isError()) return;
				
				if (MatchDetails.class.isInstance(inPacket)){
					MatchDetails mD = (MatchDetails) inPacket;
					updateMatch(mD.getMatchId(), mD.getMatchName(), mD.getMapId(), mD.getHostId());
					
				}else if (PlayerDetails.class.isInstance(inPacket)){
					PlayerDetails plD = (PlayerDetails) inPacket;
					updatePlayers(plD.getPlayerId(), plD.getPlayerName(), plD.getMapLocation(), plD.isLeft(), plD.isReady());
				}
				break;
			case START_MATCH:
				if (inPacket.isError()){
					showError(inPacket.getMessage());
					return;
				}
				
				MatchDetails.Start sM = (MatchDetails.Start) inPacket;
				startMatch(sM.getGroupAddress(), sM.getGroupPort(), sM.getServerAddress(), sM.getServerPort());
				
				break;
			case REFRESH_ROOMS:
				if (inPacket.isError()) return;
				
				receiveMatchRooms((MatchDetails.List) inPacket);
				break;
			default:
				break;
				
		}
	}
	
	protected void receiveMatchRooms(MatchDetails.List matchDetails){
		
	}
	
	protected void matchInfoUpdated(){
		
	}
	
	protected void playersUpdated(){
		
	}
	
	protected void playerLeft(String playerName){
		
	}

	@Override
	protected void renderGameState(GameContainer gc, StateBasedGame sbg, Graphics g) {}

	@Override
	protected void updateGameState(GameContainer gc, StateBasedGame sbg, int delta) {}
}
