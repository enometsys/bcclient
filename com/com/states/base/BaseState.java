package com.states.base;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.Player;
import com.connection.TCPConnectionHandler;
import com.packet.tcp.TCPPacket;
import com.utils.Util;

import de.lessvoid.nifty.slick2d.NiftyOverlayBasicGameState;

public abstract class BaseState extends NiftyOverlayBasicGameState implements TCPConnectionHandler.ConnectionInterface{
	private static final String TAG = BaseState.class.getSimpleName();

	public BaseState(int stateId) {
		super();
		this.stateId = stateId;
	}
	
	/**
	 * ========================= STATE INFO AND INITIALIZATION ======================================
	 */
	@Override
	public final int getID() {
		return stateId;
	}
	


	@Override
	protected final void initGameAndGUI(GameContainer gc, StateBasedGame sbg) {
		initNifty(gc, sbg);
	}

	
	/**
	 * =============================== UPDATE AND RENDER ============================================
	 */
	@Override
	protected final void renderGame(GameContainer gc, StateBasedGame sbg, Graphics g) {
		if (stateId == Util.STATE_GAME)
			renderGameState(gc,sbg,g);
	}
	
	@Override
	protected final void updateGame(GameContainer gc, StateBasedGame sbg, int delta) {
		this.sbg = sbg;
		if (stateId == Util.STATE_GAME)
			updateGameState(gc,sbg,delta);
	}
	
	/**
	 * Will only render/update gameState
	 */
	protected abstract void renderGameState(GameContainer gc, StateBasedGame sbg, Graphics g);
	protected abstract void updateGameState(GameContainer gc, StateBasedGame sbg, int delta);

	
	
	/**
	 * =================================== STATE CHANGE =============================================
	 */
	private boolean				isActive;
	private StateBasedGame		sbg;
	private final int			stateId;
	
	@Override
	protected final void enterState(GameContainer arg0, StateBasedGame arg1) {
		Main.log.d(TAG, "Entering " + Util.getStateName(stateId));
		EnterOrLeaveState(true);
		isActive = true;
	}
	
	@Override
	protected final void leaveState(GameContainer arg0, StateBasedGame arg1) {
		isActive = false;
		Main.log.d(TAG, "Leaving " + Util.getStateName(stateId));
		EnterOrLeaveState(false);
	}
	
	/**
	 * TODO: must close pop-ups and clear input fields
	 */
	protected abstract void EnterOrLeaveState(boolean enter);
	
	protected final void nextState(){
		/*
		 * GAME has no next state
		 * cannot go to any state unless logged in as a player
		 * 
		 * cannot go to room if no match is registered
		 * cannot go to game if match not yet started
		 */
		if (getPlayerInfo() == null){
			Main.log.e(TAG, "Cannot go to any state unless logged in as a player!");
			return;
		}
		
		switch(stateId){
			case Util.STATE_MENU:
				sbg.enterState(Util.STATE_LOBBY); 	break;
				
			case Util.STATE_LOBBY:
				if (hasJoinedMatch()){
					sbg.enterState(Util.STATE_ROOM);
				}else{
					Main.log.e(TAG, "Cannot go to Room state, no match registered!");
				}
				break;
				
			case Util.STATE_ROOM:
				if (joinedMatchStarted()){
					sbg.enterState(Util.STATE_GAME);
				}else{
					Main.log.e(TAG, "Cannot go to Game state, match not yet started!");
				}
				 	break;
			default:
				break;
		}
	}
	
	protected abstract boolean hasJoinedMatch();
	protected abstract boolean joinedMatchStarted();
	
	protected final void prevState(){
		/*
		 * Menu has no previous state
		 * Lobby needs to disconnect
		 * unregister the map
		 */
		switch(stateId){
			case Util.STATE_LOBBY:
				backToMenu();
				break;
			case Util.STATE_ROOM:
			case Util.STATE_GAME:
				unregisterJoinedMatch();
				sbg.enterState(Util.STATE_LOBBY); 	break;
			default:
				break;
		}
	}
	
	protected final void backToMenu(){
		Main.autoLogin = false;
		dontShowDisconnection = true;
		disconnectFromServer();
		sbg.enterState(Util.STATE_MENU);
	}
	
	private boolean dontShowDisconnection;
	
	protected abstract void unregisterJoinedMatch();
	
	/**
	 * ================================ CONNECTION INTERFACE ========================================
	 */
	private TCPConnectionHandler tcpConnection;
	
	@Override
	public final void TCPconnected(TCPConnectionHandler tcpConnection){
		this.tcpConnection = tcpConnection;
		connectedToServer();
	}
	
	protected void connectedToServer(){
		
	}
	
	@Override
	public final void TCPdisconnected(){
		this.tcpConnection = null;
		BaseState.playerInfo = null;
		
		if (!dontShowDisconnection)
			if (isActive)
				disconnectedFromServer();
	}
	
	protected abstract void disconnectedFromServer();
	
	@Override
	public final void sendTCPPacket(TCPPacket outPacket) {
		if (tcpConnection != null){
			tcpConnection.sendPacket(outPacket);
		}else{
			Main.log.w(TAG, "Disconnected from server! Cannot send " + outPacket.toString());
		}
	}

	@Override
	public abstract void receiveTCPPacket(TCPPacket inPacket);
	
	private void disconnectFromServer(){
		if (tcpConnection != null)
			tcpConnection.disconnect();
	}
	
	protected final void exitGame(){
		disconnectFromServer();
		Main.exitGame();
	}
	
	protected final boolean isConnectedToServer(){
		return tcpConnection != null;
	}
	
	/**
	 * ====================================== PLAYER INFORMATION ===================================
	 */
	private static Player playerInfo;
	
	protected final Player getPlayerInfo(){
		return playerInfo;
	}
	
	protected final void loginAs(int playerId, String playerName){
		if (Main.test){
			playerInfo = new Player(playerId, playerName);
		}else{
			if (stateId == Util.STATE_MENU){
				playerInfo = new Player(playerId, playerName);
				Main.log.d(TAG, "Player logged with id: " + playerId + " as " + playerName);
				nextState();
			}else{
				Main.log.e(TAG, "Cannot login from " + Util.getStateName(stateId));
			}
		}
	}
	
	public final static Player getPlayer(){
		return playerInfo;
	}
}
