package com.states;

import java.net.InetAddress;

import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.Player.MatchPlayer;
import com.packet.tcp.TCPPacketBuilder;
import com.states.Game.UDPConnector;
import com.states.base.MatchState;
import com.states.base.NiftyUIController.RoomInterf;
import com.utils.NiftyDetails;
import com.utils.Util;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.NiftyDelayedMethodInvoke;
import de.lessvoid.nifty.controls.Button;
import de.lessvoid.nifty.controls.Label;
import de.lessvoid.nifty.elements.Element;
import de.lessvoid.nifty.elements.render.ImageRenderer;

public final class Room extends MatchState implements RoomInterf{
	private static final String TAG = Room.class.getSimpleName();
	
	private final UDPConnector udpConnector;
	
	private Element[] 	playerImages;
	private Label[] 	playerNames;
	private Label[] 	playerStatus;
	
	private Label	 	matchName;
	
	private Element		matchMapImage;
	private Label	 	matchMapName;
	private Button		btnPrevMap;
	private Button		btnNextMap;
	
	private Button 		btnStartReady;
	
	public Room(UDPConnector udpConnector) {
		super(Util.STATE_ROOM);
		this.udpConnector = udpConnector;
	}
	
	@Override
	protected RoomInterf getRoomInterf() {
		return this;
	}
	
	@Override
	protected void startUDPConnection(InetAddress groupAddress, int groupPort, InetAddress serverAddress,
			int serverPort) {
		udpConnector.startUDPConnection(groupAddress, groupPort, serverAddress, serverPort);
	}

	@Override
	public void readyStartHit() {
		if (getJoinedMatch() == null) return;
		
		if (getPlayerInfo().isHost()){
			sendTCPPacket(TCPPacketBuilder.getStartMatch());
		}else{
			sendTCPPacket(TCPPacketBuilder.getChangeReady());
		}
	}
	
	@Override
	public void changeMapPrev() {
		if (getJoinedMatch() == null) return;
		
	}

	@Override
	public void changeMapNext() {
		if (getJoinedMatch() == null) return;
		
	}
	
	@Override
	public void leaveMatch() {
		sendTCPPacket(TCPPacketBuilder.getLeaveMatch());
	}

	@Override
	protected void additionalElementsAfterChat(Nifty nifty, StateBasedGame sbg) {
		playerImages = new Element[5];
		playerNames = new Label[5];
		playerStatus = new Label[5];
		
		for(int i=0; i<5; i++){
			playerImages[i] = nifty.getScreen(NiftyDetails.K.SCRN_START).findElementById(NiftyDetails.K.IMG_PLAYER[i]);
			playerNames[i] = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.LBL_PLAYER_NAME[i], Label.class);
			playerStatus[i] = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.LBL_PLAYER_STATUS[i], Label.class);
		}
		
		matchName = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.LBL_MATCH_NAME, Label.class);

		matchMapImage = nifty.getScreen(NiftyDetails.K.SCRN_START).findElementById(NiftyDetails.K.IMG_MAP);
		matchMapName = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.LBL_MAP_NAME, Label.class);
		btnPrevMap = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.BTN_PREV, Button.class);
		btnNextMap = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.BTN_NEXT, Button.class);
		
		btnStartReady = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.BTN_START_READY, Button.class);
	}
	
	@Override
	protected final void matchInfoUpdated(){
		getNifty().delayedMethodInvoke(new NiftyDelayedMethodInvoke(){
			@Override
			public void performInvoke(Object... invokeParametersParam) {
				redrawMatchRoom();
			}
		}, (Object[]) null);
	}
	
	@Override
	protected final void playersUpdated(){
		getNifty().delayedMethodInvoke(new NiftyDelayedMethodInvoke(){
			@Override
			public void performInvoke(Object... invokeParametersParam) {
				redrawPlayers();
			}
		}, (Object[]) null);
	}
	
	private void redrawPlayers(){
		Nifty nifty = this.getNifty();
		int index = 1;
		
		Main.log.d(TAG, "Redrawing player details...");
		
		//Set player details: images, names, ready state
		for(MatchPlayer pl:getJoinedMatch().getPlayers()){
			int ind = (pl.isSame(getPlayerInfo()))? 0:index++;
			
			playerImages[ind].getRenderer(ImageRenderer.class).setImage(
					nifty.getRenderEngine().createImage(nifty.getScreen(NiftyDetails.K.SCRN_START),
							(pl.isHost())? NiftyDetails.K.PATH_IMG_HOST:NiftyDetails.K.PATH_IMG_DEFAULT, false));
			
			playerNames[ind].setText(pl.getPlayerName());
			playerStatus[ind].setText(pl.isHost()? 
					NiftyDetails.K.CAPT_HOST:(pl.isReady())? NiftyDetails.K.CAPT_READY:NiftyDetails.K.CAPT_NOT_READY);
		}
		
		//fill remaining with blanks/place holders
		for(int i=index; i<5; i++){
			playerImages[i].getRenderer(ImageRenderer.class).setImage(
					nifty.getRenderEngine().createImage(nifty.getScreen(NiftyDetails.K.SCRN_START), NiftyDetails.K.PATH_IMG_BLANK, false));
			playerNames[i].setText("");
			playerStatus[i].setText("");
		}

		//set ready/start button label
		if (getPlayerInfo().isHost()){
			btnStartReady.setText(NiftyDetails.K.CAPT_START);
			btnStartReady.setEnabled(getJoinedMatch().canStart());			
		}else{
			//show ready/not ready button
			btnStartReady.setText((!getJoinedMatch().getPlayer(getPlayerInfo().getPlayerId()).isReady())? NiftyDetails.K.CAPT_READY:NiftyDetails.K.CAPT_CANCEL_READY);
			btnStartReady.setEnabled(true);	
		}
	}
	
	private void redrawMatchRoom(){
		Nifty nifty = this.getNifty();
		
		Main.log.d(TAG, "Redrawing room details...");
		
		matchName.setText(getJoinedMatch().getMatchName());
		
		//TODO: update matchName and image
		matchMapImage.getRenderer(ImageRenderer.class).setImage(
				nifty.getRenderEngine().createImage(nifty.getScreen(NiftyDetails.K.SCRN_START), 
						NiftyDetails.K.PATH_MAP_DEFAULT, false));
		matchMapName.setText(NiftyDetails.K.NAME_MAP_DEFAULT);
		
		btnPrevMap.setEnabled(false);
		btnNextMap.setEnabled(false);
	}
}
