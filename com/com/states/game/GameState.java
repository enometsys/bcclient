package com.states.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.objects.GameObject;
import com.objects.gamecharacter.GameCharacter;
import com.packet.udp.Action;
import com.utils.Outliner;

public abstract class GameState extends GameObjectState{
	//private static final String TAG = GameState.class.getSimpleName();
	
	//PlayerId				//ObjectId	ActionUpdate
	private Map<Integer, Map<Integer, Action>> 				gameObjectUpdates;
	
	//playerId				ObjectId
	private Map<Integer, ArrayList<Integer>>				deadObjects;
	
	public GameState() {
		super();
		gameObjectUpdates = new HashMap<Integer, Map<Integer, Action>>();
		deadObjects = new HashMap<Integer, ArrayList<Integer>>();
		selectedObjects = new ArrayList<GameObject>();
		canSelect = true;
	}

	@Override
	protected void playerInitialized(int playerId) {
		if (!deadObjects.containsKey(playerId))
			deadObjects.put(playerId, new ArrayList<Integer>());
		
		if (!gameObjectUpdates.containsKey(playerId))
			gameObjectUpdates.put(playerId, new HashMap<Integer, Action>());
		
		if (getJoinedMatch().isAllInitialized()) gameStart();
	}

	private void gameStart() {
		togglePause(false);
	}

	@Override
	protected synchronized void dealWithObjectUpdates() {
		/*for(Integer playerId: gameObjectUpdates.keySet()){
			
			Iterator<Integer> updIter = gameObjectUpdates.get(playerId).keySet().iterator();
			while(updIter.hasNext()){
				int objectId = updIter.next();
				
				if (!deadObjects.get(playerId).contains(objectId) || Main.test){
					GameObject go = getObject(playerId, objectId);
					
					if (go!=null){
						go.updateObject(gameObjectUpdates.get(playerId).get(objectId));
						updIter.remove();
					}else{
						spawnUpdateObject(gameObjectUpdates.get(playerId).get(objectId));
						updIter.remove();
					}
				}
			}
		}
		
		gameObjectUpdates.clear();*/
	}

	@Override
	protected void objectWentInactive(GameObject go) {
		deadObjects.get(go.getPlayerId()).add(go.getObjectId());
	}
	
	protected synchronized final void receiveUpdate(Action update){
		if (deadObjects.containsKey(update.getPlayerId())){
			if (deadObjects.get(update.getPlayerId()).contains(update.getGameObjectId()) || Main.test)
				return;
		}
		GameObject go = getObject(update.getPlayerId(), update.getGameObjectId());
		if (go!=null){
			go.updateObject(update);
		}else{
			spawnUpdateObject(update);
		}
	}
	
	@Override
	public void leaveMatch() {
		super.leaveMatch();
		gameObjectUpdates.clear();
		deadObjects.clear();
		canSelect = true;
	}

	@Override
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int xLoc, int lastYLoc) {
		lastYLoc = super.renderDebug(gc, sbg, g, xLoc, lastYLoc);
		
		g.drawString("Can select:   " + canSelect, xLoc, lastYLoc += 20);
		g.drawString("Selecting :   " + selecting, xLoc, lastYLoc += 20);
		g.drawString("Selected objects :   " + selectedObjects.size(), xLoc, lastYLoc += 20);
		g.drawString("All objects :   " + getAllObjects(true, null).size(), xLoc, lastYLoc += 20);
		g.drawString("Updates   :   " + gameObjectUpdates.size(), xLoc, lastYLoc += 20);
		
		return lastYLoc;
	}	
	
	private void rightClickAction(GameObject s, int mapX, int mapY, int mouseX, int mouseY, GameObject target) {
		if (GameCharacter.class.isInstance(s)){
			GameCharacter ns = (GameCharacter) s;
			ns.rightClickAction(mouseX - mapX, mouseY - mapY, target);
		}
	}

	/**
	 * ====================================== OBJECT SELECTION ========================================
	 */
	private static final int			MAX_SELECTIONS = 12;
	
	private ArrayList<GameObject> 	selectedObjects;
	private boolean 					selecting;
	private Shape 						selectionRectangle;
	private int 						selStartX, selStartY;
	
	private boolean						canSelect;
	private boolean						rightClicking;
	private RightClickObject			rightClickObject;
	
	@Override
	protected void renderObjectSelector(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY) {
		if (rightClicking){
			//TODO: play right click Animation
			
			switch(rightClickObject){
				case ALLY:
					break;
				case ENEMY:
					break;
				case GROUND:
					break;
				default:
					break;
			}
			
			rightClicking = false;
		}
		
		if (selecting) g.draw(selectionRectangle);
	}

	@Override
	protected void updateObjectSelector(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		if (!canSelect) return;
		
		Input input = gc.getInput();
		int mouseX = input.getMouseX();
		int mouseY = input.getMouseY();
		
		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)){
			clearSelection();
			Shape singleObjectSelector = new Circle(mouseX, mouseY, 3);
			for(GameObject s: getAllyUnits(getPlayerInfo().getPlayerId(), true, null)){
				if (singleObjectSelector.intersects(Outliner.getVisibleOutline(s.getOutline(), mapX, mapY))){
					selectObject(s); break;
				}
			}
		}else if (input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)){
			if (selectedObjects.size() > 1) clearSelection();
			
			selecting = true;
			selectionRectangle = new Rectangle(selStartX, selStartY, mouseX-selStartX, mouseY-selStartY);
			for(GameObject s: getAllyUnits(getPlayerInfo().getPlayerId(), true, null)){
				if (selectionRectangle.intersects(Outliner.getVisibleOutline(s.getOutline(), mapX, mapY))){
					if (selectedObjects.size() < MAX_SELECTIONS)
						selectObject(s);
					else break;
				}
			}
			
		}else if (input.isMousePressed(Input.MOUSE_RIGHT_BUTTON)){
			rightClicking = true;
			rightClickObject = RightClickObject.GROUND;
					
			if (selectedObjects != null){
				GameObject target = null;
				
				Shape actionSelector = new Circle(mouseX, mouseY, 4);
				for(GameObject s: getAllObjects(true, null)){
					if (actionSelector.intersects(Outliner.getVisibleOutline(s.getOutline(), mapX, mapY))){
						target = s;
						
						if (s.isEnemy()){
							rightClickObject = RightClickObject.ENEMY;
						}else{
							rightClickObject = RightClickObject.ALLY;
						}
						
						break;
					}
				}
				
				for(GameObject s: selectedObjects){
					if (s.equals(target)) target = null;
					rightClickAction(s, mapX, mapY, mouseX, mouseY, target);
				}
			}
		}else{
			selecting = false;
			selStartX = mouseX;
			selStartY = mouseY;
		}
	}

	private void selectObject(GameObject obj){
		if (!selectedObjects.contains(obj))
			selectedObjects.add(obj);
		obj.selected(true);
	}
	
	private void clearSelection(){
		for(GameObject obj:selectedObjects){
			obj.selected(false);
		}
		
		selectedObjects.clear();
	}
	
	protected final void toggleCanSelect(boolean canSelect){
		this.canSelect = canSelect;
	}
	
	protected final void toggleCanSelect(){
		toggleCanSelect(!canSelect);
	}
	
	public enum RightClickObject{
		ENEMY, ALLY, GROUND
	}
}
