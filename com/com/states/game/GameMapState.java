package com.states.game;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

import com.Main;
import com.Player.MatchPlayer;
import com.objects.GameObject;
import com.objects.map.GameMap;
import com.utils.Util;

public abstract class GameMapState extends BaseGameState{
	
	private boolean		initializeMap;
	private int 		mapId;
	private	int			scrollSpeed, scrollSensitivity;
	private int			lowXBound, lowYBound;
	private int			uppXBound, uppYBound;
	
	private GameMap	gameMap;
	private int 		mapX, mapY;
	
	private boolean 	scrollable;
	
	private boolean		renderMapGeo;
	
	
	
	public GameMapState() {
		super();
		scrollable 		= true;
		renderMapGeo 	= false;
	}

	@Override
	public void leaveMatch() {
		initializeMap = false;
		gameMap = null;
		mapX = mapY = 0;
		scrollable = true;
		renderMapGeo = false;
	}

	@Override
	public void gameOver(int winnderId) {
		MatchPlayer winnerDetails = getJoinedMatch().getPlayer(winnderId);
		
		if (winnerDetails != null){
			mapX = gameMap.getMapXCoord(winnerDetails.getMapLocation());
			mapY = gameMap.getMapXCoord(winnerDetails.getMapLocation());
		}
		
		toggleScrolling(true);
		super.gameOver(winnderId);
	}

	@Override
	protected final boolean isMapInitialized() {
		return gameMap != null;
		
	}
	
	@Override
	protected final void mapInitializer() {
		if (initializeMap){
			
			int myPlayerId = getPlayerInfo().getPlayerId();
			initializePlayer(myPlayerId);
			
			gameMap = new GameMap(mapId, scrollSpeed);
			gameMap.initPlayerBase(getJoinedMatch(), getGameConnection(), myPlayerId);
			
			lowXBound = gameMap.getMarLowX(scrollSpeed);
			lowYBound = gameMap.getMarLowY(scrollSpeed);
			
			uppXBound = gameMap.getMarUpX(scrollSpeed);
			uppYBound = gameMap.getMarUpY(scrollSpeed);
			
			mapX = gameMap.getMapXCoord(getJoinedMatch().getPlayer(myPlayerId).getMapLocation());
			mapY = gameMap.getMapYCoord(getJoinedMatch().getPlayer(myPlayerId).getMapLocation());
			
			mapInitialized();
		}		
	}
	
	protected abstract void mapInitialized();
	protected abstract void initializePlayer(int playerId);
	
	protected final void initializeMap(int mapId, int scrollSpeed, int scrollSensitivity){
		this.mapId = mapId;
		this.scrollSpeed = scrollSpeed;
		this.scrollSensitivity = scrollSensitivity;		
		this.initializeMap = true;
	}
	
	protected abstract NewGameConnection getGameConnection();

	@Override
	protected final void renderAll(GameContainer gc, StateBasedGame sbg, Graphics g) {
		g.setWorldClip(new Rectangle( 10, 10, Util.X_SIZE - 20, Util.Y_SIZE - 20));
		
		gameMap.renderGame(gc, sbg, g, mapX, mapY, renderMapGeo || Main.test);
		
		renderObjects(gc, sbg, g, mapX, mapY);
	}

	@Override
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int xLoc, int lastYLoc) {
		lastYLoc = super.renderDebug(gc, sbg, g, xLoc, lastYLoc);
		
		g.drawString("MouseCoords    : " + gc.getInput().getMouseX() + ", " + gc.getInput().getMouseY(), xLoc, lastYLoc += 20);
		g.drawString("AbsMouseCoords : " + gc.getInput().getAbsoluteMouseX() + ", " + gc.getInput().getAbsoluteMouseY(), xLoc, lastYLoc += 20);
		g.drawString("Scrollable     : " + scrollable, xLoc, lastYLoc += 20);
		g.drawString("RefPoints      : " + mapX + ", " + mapY, xLoc, lastYLoc += 20);
		
		return lastYLoc;
	}

	protected abstract void renderObjects(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY);

	@Override
	protected final void updateAll(GameContainer gc, StateBasedGame sbg, int delta) {
		if (scrollable) mapScroll(gc, gc.getInput(), gc.getInput().getMouseX(), gc.getInput().getMouseY(), gc.getWidth(), gc.getHeight());
		
		updateObjects(gc, sbg, delta, mapX, mapY);
	}
	
	protected abstract void updateObjects(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY);
	
	private void mapScroll(GameContainer gc, Input input, int mouseX, int mouseY, int scrW, int scrH){
		if (mouseY < scrollSensitivity 			&& mapY < lowYBound) 	mapY += scrollSpeed;
		if (mouseY > scrH - scrollSensitivity 	&& mapY > uppYBound) 	mapY -= scrollSpeed;
		
		if (mouseX < scrollSensitivity 			&& mapX < lowXBound) 	mapX += scrollSpeed;
		if (mouseX > scrW - scrollSensitivity 	&& mapX > uppXBound)	mapX -= scrollSpeed;
		
		if (input.isKeyDown(Input.KEY_UP) 		&& mapY < lowYBound) 	mapY += scrollSpeed;
		if (input.isKeyDown(Input.KEY_DOWN) 	&& mapY > uppYBound)  	mapY -= scrollSpeed;
		
		if (input.isKeyDown(Input.KEY_LEFT) 	&& mapX < lowXBound)  	mapX += scrollSpeed;
		if (input.isKeyDown(Input.KEY_RIGHT) 	&& mapX > uppXBound)  	mapX -= scrollSpeed;
	}

	protected final void toggleScrolling(boolean scrollable){
		this.scrollable = scrollable;
	}
	
	protected final void toggleScrolling(){
		toggleScrolling(!scrollable);
	}

	protected final void toggleRenderGeo(boolean renderMapGeo){
		this.renderMapGeo = renderMapGeo;
	}
	
	protected final void toggleRenderGeo(){
		toggleRenderGeo(!renderMapGeo);
	}

	public ObstaclesManager getObstaclesManager() {
		return gameMap;
	}
	
	public TileBasedMap getMap(){
		return gameMap;
	}
	
	public static interface ObstaclesManager{
		void addToObstacles(GameObject gameObject);
		void removeFromObstacles(GameObject gameObject);
		boolean	withinBorder(int x, int y);
		boolean isBlocked(Line ln);
	}
}
