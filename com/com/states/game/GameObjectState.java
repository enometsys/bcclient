package com.states.game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.Player.MatchPlayer;
import com.objects.GameObject;
import com.objects.gamecharacter.GameCharacter;
import com.objects.utils.BarracksDetails;
import com.objects.utils.ObjectDetails;
import com.packet.udp.Action;
import com.packet.udp.UDPPacket;

public abstract class GameObjectState extends GameMapState implements NewGameConnection{
	private static final String TAG = GameObjectState.class.getSimpleName();
	
	//playerId			    	mobileUnit	objectId	object
	protected Map<Integer, Map<Boolean, Map<Integer, GameObject>>> 	gameObjects;
	
	private ArrayList<GameObject> 						objectSpawnQueue;
	private ArrayList<Integer>								playerInitializationQueue;
	
	private boolean											renderObjectGeo;
	private boolean											paused;
	private boolean											gameOver;
	
	public GameObjectState() {
		super();
		
		gameObjects 				= new HashMap<Integer, Map<Boolean, Map<Integer, GameObject>>>();
	
		objectSpawnQueue 			= new ArrayList<GameObject>();
		playerInitializationQueue 	= new ArrayList<Integer>();
		
		renderObjectGeo		= false;
		gameOver			= false;
		paused				= true;
	}
	

	@Override
	protected final NewGameConnection getGameConnection() {
		return this;
	}
	
	@Override
	public void gameOver(int winnderId) {
		gameOver = true;
		paused	= true;
		super.gameOver(winnderId);
	}
	
	protected final void togglePause(boolean pause){
		this.paused = pause;
	}
	
	protected final void togglePause(){
		togglePause(!paused);
	}

	@Override
	public void leaveMatch() {
		super.leaveMatch();
		
		gameObjects.clear();
		objectSpawnQueue.clear();
		playerInitializationQueue.clear();
		
		renderObjectGeo = false;
		gameOver		= false;
		paused			= true;
	}

	@Override
	protected final void renderObjects(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY) {
		for(Integer playerId: gameObjects.keySet()){
			for(Boolean unitObject: gameObjects.get(playerId).keySet()){
				for(GameObject go: gameObjects.get(playerId).get(unitObject).values()){
					go.renderObject(gc, sbg, g, mapX, mapY, renderObjectGeo || Main.test);
				}
			}
		}
		
		renderObjectSelector(gc, sbg, g, mapX, mapY);
	}
	
	protected abstract void renderObjectSelector(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY);

	@Override
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int xLoc, int lastYLoc) {
		lastYLoc = super.renderDebug(gc, sbg, g, xLoc, lastYLoc);
		
		for(MatchPlayer pl: getJoinedMatch().getInitializedPlayers()){
			g.drawString("Player " + pl.getPlayerName() +
					"'s Unit objects: " + gameObjects.get(pl.getPlayerId()).get(true).size() +
					" NonUnit objects: " + gameObjects.get(pl.getPlayerId()).get(false).size(), xLoc, lastYLoc += 20);
		}
		
		g.drawString("SpawnQueue      : " + objectSpawnQueue.size(), xLoc, lastYLoc += 20);
		g.drawString("PlayerInitQueue : " + playerInitializationQueue.size(), xLoc, lastYLoc += 20);
		
		return lastYLoc;
	}

	@Override
	protected void updateObjects(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		
		if (gameOver) return;
		
		Iterator<Integer> pInit = playerInitializationQueue.iterator();
		while(pInit.hasNext()){
			initializePlayer(pInit.next());
			pInit.remove();
		}
		
		//dealWithObjectUpdates();
		
		Iterator<GameObject> oSpwn = objectSpawnQueue.iterator();
		while(oSpwn.hasNext()){
			GameObject go = oSpwn.next();
			gameObjects.get(go.getPlayerId()).get(GameCharacter.class.isInstance(go)).put(go.getObjectId(), go);
			oSpwn.remove();
		}
		
		if (paused) return;
		
		for(Integer playerId: gameObjects.keySet()){
			for(Boolean unitObject: gameObjects.get(playerId).keySet()){
				Iterator<GameObject> r = gameObjects.get(playerId).get(unitObject).values().iterator();
				while(r.hasNext()){
					GameObject go = r.next();
					
					if (go.isActive()){
						go.updateObject(gc, sbg, delta, mapX, mapY);
					}else{
						objectWentInactive(go);
						r.remove();
					}
				}
			}
		}
		
		updateObjectSelector(gc, sbg, delta, mapX, mapY);
	}
	
	protected abstract void dealWithObjectUpdates();
	
	protected abstract void updateObjectSelector(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY);
	
	protected abstract void objectWentInactive(GameObject go);
	
	/**
	 * ====================================== OBJECT GETTERS ========================================
	 */
	@Override
	public final ArrayList<GameObject> getEnemyUnits(int enemyOfPlayerId, boolean mobileUnitsOnly,
			GameObject excluding) {
		ArrayList<GameObject> desireCollection = new ArrayList<GameObject>();
		
		for(Integer playerId: gameObjects.keySet()){
			if (playerId == enemyOfPlayerId) continue;
			for(Boolean isUnitObject: gameObjects.get(playerId).keySet()){
				if (mobileUnitsOnly && isUnitObject.booleanValue() != mobileUnitsOnly) continue;
				for(GameObject go: gameObjects.get(playerId).get(isUnitObject).values()){
					if (!go.equals(excluding))
						desireCollection.add(go);
				}				
			}
		}
		
		return desireCollection;
	}

	@Override
	public final ArrayList<GameObject> getAllyUnits(int playerId, boolean mobileUnitsOnly, GameObject excluding) {
		ArrayList<GameObject> desireCollection = new ArrayList<GameObject>();
		
		if (gameObjects.containsKey(playerId)){
			for(Boolean isUnitObject: gameObjects.get(playerId).keySet()){
				if (isUnitObject.booleanValue() != mobileUnitsOnly) continue;
				for(GameObject go: gameObjects.get(playerId).get(isUnitObject).values()){
					if (!go.equals(excluding))
						desireCollection.add(go);
				}
			}
		}
		
		return desireCollection;
	}

	@Override
	public final GameObject getTargetObject(int targetObjectPlayerId, int targetObjectId) {
		return getObject(targetObjectPlayerId, targetObjectId);
	}
	
	protected final GameObject getObject(int playerId, int objectId){
		if (gameObjects.containsKey(playerId)){
			for(Boolean isUnitObject: gameObjects.get(playerId).keySet()){
				if (gameObjects.get(playerId).get(isUnitObject).containsKey(objectId)){
					return gameObjects.get(playerId).get(isUnitObject).get(objectId);
				}
			}
		}
		return null;
	}
	
	protected final ArrayList<GameObject> getAllObjects(boolean mobileUnitsOnly, GameObject excluding){
		return getEnemyUnits(-1, mobileUnitsOnly, excluding);
	}
	/**
	 * ====================================== OBJECT GETTERS ========================================
	 */
	protected final void addToPlayerInitializationQueue(int playerId){
		if (!playerInitializationQueue.contains(playerId)){
			MatchPlayer plDetails = getJoinedMatch().getPlayer(playerId);
			
			if (plDetails != null){
				if (!plDetails.isInitialized()){
					playerInitializationQueue.add(playerId);
				}
			}else{
				Main.log.e(TAG, "Error adding player " + playerId + " to playerInitializationQueue. player not member of match");
			}
		}
	}
	
	protected final void initializePlayer(int playerId){
		MatchPlayer playerDetails = getJoinedMatch().getPlayer(playerId);
		
		if (playerDetails != null){
			gameObjects.put(playerId, new HashMap<Boolean, Map<Integer, GameObject>>());
			gameObjects.get(playerId).put(true, new HashMap<Integer, GameObject>());
			gameObjects.get(playerId).put(false, new HashMap<Integer, GameObject>());
			
			playerDetails.initializePlayer();
			
			playerInitialized(playerId);
			BarracksDetails.startPlayer(playerDetails.getPlayerId());
			
			Main.log.d(TAG, "Player " + playerDetails.getPlayerName() + " initialized!");
		}else{
			Main.log.e(TAG, "Error initializing player " + playerId + ". player not member of match");
		}
	}
	
	protected abstract void playerInitialized(int playerId);

	/**
	 * ====================================== OBJECT SPAWNER ========================================
	 */
	@Override
	public final void addToSpawnQueue(GameObject go) {
		objectSpawnQueue.add(go);
	}
	
	protected final void toggleObjectGeoRendering(boolean renderObjectGeo){
		this.renderObjectGeo = renderObjectGeo;
	}
	
	protected final void toggleObjectGeoRendering(){
		toggleObjectGeoRendering(!renderObjectGeo);
	}
	
	protected final void spawnUpdateObject(Action update){
		addToSpawnQueue(ObjectDetails.spawnUpdateObject(this, 
				update.getPlayerId(), update.getGameObjectId(), update.getGameObjectTypeId(), update));
	}
	
	@Override
	public abstract void updateObject(UDPPacket dPckt);
}
