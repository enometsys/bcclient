package com.states.game;

import java.util.ArrayList;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.state.StateBasedGame;
import com.states.base.NiftyUIController.GameInterf;
import com.utils.NiftyDetails;
import com.utils.Util;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.controls.Label;

import com.Main;
import com.objects.utils.BarracksDetails;
import com.objects.utils.ObjectDetails.K;
import com.states.base.MatchState;

public abstract class BaseGameState extends MatchState implements GameInterf{
	
	private boolean 	onLoadingScreen;
	private boolean 	renderDebug;
	
	private Label[] 	gameCharCount;
	
	public BaseGameState() {
		super(Util.STATE_GAME);
		onLoadingScreen = true;
	}
	
	@Override
	public void leaveMatch() {
		onLoadingScreen = true;
		renderDebug = false;
	}	
	
	public void gameOver(int winnderId){
		this.showPrompt("GameOver! the winner is " + getJoinedMatch().getPlayer(winnderId).getPlayerName());
	}
	
	@Override
	protected GameInterf getGameInterf() {
		return this;
	}

	@Override
	protected ArrayList<String> additionalPopups(ArrayList<String> additionalPopups) {
		additionalPopups.add(NiftyDetails.K.POP_CHAT);
		additionalPopups.add(NiftyDetails.K.POP_MENU);
		return additionalPopups;
	}

	@Override
	public void showOptions() {
		// TODO Auto-generated method stub
	}

	@Override
	public void keyPressed(int key, char c) {
		switch(key){
			case Input.KEY_TAB:
				togglePopup(NiftyDetails.K.POP_CHAT);
				return;
			case Input.KEY_ESCAPE:
				togglePopup(NiftyDetails.K.POP_MENU);
				return;
		}
		
		super.keyPressed(key, c);
	}	

	@Override
	protected final void renderGameState(GameContainer gc, StateBasedGame sbg, Graphics g) {
		if (g.getWorldClip() == null){
			g.setWorldClip(new Rectangle( 10, 10, Util.X_SIZE - 20, Util.Y_SIZE - 20));
		}
		
		if (onLoadingScreen){
			mapInitializer();
			
			if (isMapInitialized()){
				onLoadingScreen = false;
				gotoScreen(NiftyDetails.K.SCRN_START);
			}
		}else{
			renderAll(gc, sbg, g);
		}
		
		if (renderDebug || Main.test){
			g.setColor(Color.white);
			renderDebug(gc, sbg, g, 50, 140);
		}
	}
	
	protected abstract void mapInitializer();
	protected abstract boolean isMapInitialized();
	protected abstract void renderAll(GameContainer gc, StateBasedGame sbg, Graphics g);
	
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, final int xLoc, int lastYLoc){
		
		g.drawString("PlayerName		 : " + getPlayerInfo().getPlayerId() + " - " + getPlayerInfo().getPlayerName(), xLoc, lastYLoc += 20);
		g.drawString("MapLocation		 : " + getJoinedMatch().getPlayer(getPlayerInfo().getPlayerId()).getMapLocation(), xLoc, lastYLoc += 20);
		
		
		return lastYLoc;
	}
	
	@Override
	protected final void updateGameState(GameContainer gc, StateBasedGame sbg, int delta) {
		if (onLoadingScreen){
			
		}else{
			updateAll(gc, sbg, delta);
			
			updateUpperRight();
		}
	}	

	@Override
	protected void additionalElementsAfterChat(Nifty nifty, StateBasedGame sbg) {
		gameCharCount = new Label[7];
		
		for(int i=0; i<7; i++){
			gameCharCount[i] = nifty.getScreen(NiftyDetails.K.SCRN_START).findNiftyControl(NiftyDetails.K.LBL_CHAR_COUNT[i], Label.class);
		}
		
	}

	private void updateUpperRight() {
		int playerId = getPlayerInfo().getPlayerId();
		
		gameCharCount[0].setText(BarracksDetails.getCount(playerId, K.unit_barbarian) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_barbarian));
		
		gameCharCount[1].setText(BarracksDetails.getCount(playerId, K.unit_archer) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_archer));
		
		gameCharCount[2].setText(BarracksDetails.getCount(playerId, K.unit_wizard) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_wizard));
		
		gameCharCount[3].setText(BarracksDetails.getCount(playerId, K.unit_wallbreaker) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_wallbreaker));

		gameCharCount[4].setText(BarracksDetails.getCount(playerId, K.unit_hogrider) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_hogrider));
		
		gameCharCount[5].setText(BarracksDetails.getCount(playerId, K.unit_giant) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_giant));
		
		gameCharCount[6].setText(BarracksDetails.getCount(playerId, K.unit_dragon) + " / " + 
				BarracksDetails.getMax(playerId, K.unit_dragon));
	}

	protected abstract void updateAll(GameContainer gc, StateBasedGame sbg, int delta);
	
	protected final void toggleRenderDebug(boolean renderDebug){
		this.renderDebug = renderDebug;
	}
	
	protected final void toggleRenderDebug(){
		toggleRenderDebug(!renderDebug);
	}
}
