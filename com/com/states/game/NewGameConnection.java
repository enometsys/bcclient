package com.states.game;

import java.util.ArrayList;

import org.newdawn.slick.util.pathfinding.TileBasedMap;

import com.objects.GameObject;
import com.packet.udp.UDPPacket;
import com.states.game.GameMapState.ObstaclesManager;

public interface NewGameConnection {
	
	ArrayList<GameObject> getEnemyUnits(int enemyOfPlayerId, boolean mobileUnitsOnly, GameObject excluding);
	ArrayList<GameObject> getAllyUnits(int playerId, boolean mobileUnitsOnly, GameObject excluding);
	
	GameObject getTargetObject(int targetObjectPlayerId, int targetObjectId);
	
	void updateObject(UDPPacket dPckt);
	
	ObstaclesManager getObstaclesManager();
	void addToSpawnQueue(GameObject go);
	void gameOver(int playerId);
	
	TileBasedMap getMap();
}
