package com.utils;

import com.packet.Packet;

public class Logger {	
	public void d(String TAG, String message) {
		System.out.println("Debug     : " + TAG + ": " + message);
	}

	public void e(String TAG, String message) {
		System.out.println("Error     : " + TAG + ": " + message);
	}

	public void w(String TAG, String message) {
		System.out.println("Warning   : " + TAG + ": " + message);
	}
	
	public void r(String TAG, Packet pckt) {
		if (pckt.getPacketType() == Packet.PACKET_TYPE.TCPPacket)
			System.out.println("--- TCP Received --- " + pckt.toString());
		else
			System.out.println("--- UDP Received --- " + pckt.toString());
	}
	
	public void s(String TAG, Packet pckt) {
		if (pckt.getPacketType() == Packet.PACKET_TYPE.TCPPacket)
			System.out.println("----- TCP Sent ----- " + pckt.toString());
		//else
			//System.out.println("----- UDP Sent ----- " + pckt.toString());
	}
}
