package com.utils;

import com.Main;

public class NiftyDetails {
	private static final String TAG = NiftyDetails.class.getSimpleName();
	
	public static String getNiftyUIPath(int stateId){
		switch(stateId){
			case Util.STATE_MENU:	return K.PATH_MENU;
			case Util.STATE_LOBBY:	return K.PATH_LOBBY;
			case Util.STATE_ROOM:	return K.PATH_ROOM;
			case Util.STATE_GAME:	return K.PATH_GAME;
		}
		
		Main.log.e(TAG, "Error getting nifty screen path. Invalid id of " + stateId);
		return null;
	}
	
	public static String getStartScreen(int stateId){
		switch(stateId){
			case Util.STATE_MENU:
			case Util.STATE_LOBBY:
			case Util.STATE_ROOM:	return K.SCRN_START;
			case Util.STATE_GAME:	return K.SCRN_LOAD;
		}
		
		Main.log.e(TAG, "Error getting start screen. Invalid id of " + stateId);
		return null;
	}
	
	public static final class K{
		public static final String PATH_POPUPS 	= "res/nifty_screens/nifty-popups.xml";
		
		public static final String PATH_MENU 	= "res/nifty_screens/nifty-menu.xml";
		public static final String PATH_LOBBY 	= "res/nifty_screens/nifty-lobby.xml";
		public static final String PATH_ROOM 	= "res/nifty_screens/nifty-room.xml";
		public static final String PATH_GAME 	= "res/nifty_screens/nifty-game.xml";
		
		public static final String SCRN_LOAD 	= "load";
		public static final String SCRN_START 	= "start";
		
		public static final String POP_PROMPT 	= "popPrompt";		//yes no choice
		public static final String POP_WAIT 	= "popWait";		//no button
		public static final String POP_CHOICE 	= "popChoice"; 	//ok button
		public static final String POP_INPUT 	= "popInput"; 	//ok button
		
		public static final String POP_CHAT 	= "popChat";
		public static final String POP_MENU 	= "popMenu";
		
		public static final String POP_LBL_PROMPT 	= "lblPrompt";
		public static final String POP_LBL_WAIT 	= "lblWait";
		public static final String POP_LBL_CHOICE 	= "lblChoice";
		public static final String POP_LBL_MARK 	= "lblMark";
		public static final String POP_LBL_INPUT 	= "lblInput";
		public static final String POP_TXT_INPUT 	= "txtInput";
		public static final String POP_BTN_OK 		= "btnOk";
		public static final String POP_BTN_YES 		= "btnYes";
		public static final String POP_BTN_NO 		= "btnNo";
		public static final String POP_BTN_ENTER	= "btnEnter";
		public static final String POP_BTN_CANCEL	= "btnCancel";
		
		public static final String QRY_BACK			= "back";
		public static final String QRY_BACK_MENU	= "backToMenu";
		public static final String QRY_EXIT			= "exit";
		public static final String QRY_JOIN			= "join";
		
		public static final String LST_CHAT_BOX 		= "lstChatBox";
		public static final String TXT_CHAT_INPUT 		= "txtChat";
		
		public static final String LST_ROOM_LIST 		= "lstRoomList";
		
		
		public static final String LBL_MATCH_NAME 		= "lblMatchName";
		public static final String LBL_MAP_NAME 		= "lblMapName";
		
		public static final String IMG_MAP 				= "ImgSelectedMap";
		
		public static final String BTN_PREV 			= "btnMapPrev";
		public static final String BTN_NEXT 			= "btnMapNext";
		public static final String BTN_START_READY 		= "btnReadyStart";
	
		public static final String MAP_CHANGE_PREV 		= "prev";
		public static final String MAP_CHANGE_NEXT 		= "next";
		
		public static final String CAPT_READY 			= "Ready";
		public static final String CAPT_NOT_READY 		= "Not Ready";
		public static final String CAPT_CANCEL_READY 	= "Cancel Ready";
		public static final String CAPT_START 			= "Start Match";
		public static final String CAPT_HOST 			= "Host";
		
		public static final String PATH_IMG_HOST 		= "res/thumbs/PlayerImg/host.png";
		public static final String PATH_IMG_DEFAULT 	= "res/thumbs/PlayerImg/default.png";
		public static final String PATH_IMG_BLANK 		= "res/thumbs/PlayerImg/blank.png";
		
		public static final String PATH_MAP_DEFAULT 	= "res/thumbs/maps/map-default.png"; //temporary, put path to Map o
		public static final String NAME_MAP_DEFAULT 	= "Default Map";
		
		public static final String[] IMG_PLAYER 		= new String[5];
		public static final String[] LBL_PLAYER_NAME 	= new String[5];
		public static final String[] LBL_PLAYER_STATUS 	= new String[5];
		
		public static final String[] LBL_CHAR_COUNT 	= new String[7];
		
		static{
			IMG_PLAYER[0] = "Pl1Img";
			IMG_PLAYER[1] = "Pl2Img";
			IMG_PLAYER[2] = "Pl3Img";
			IMG_PLAYER[3] = "Pl4Img";
			IMG_PLAYER[4] = "Pl5Img";
			
			LBL_PLAYER_NAME[0] = "Pl1Name";
			LBL_PLAYER_NAME[1] = "Pl2Name";
			LBL_PLAYER_NAME[2] = "Pl3Name";
			LBL_PLAYER_NAME[3] = "Pl4Name";
			LBL_PLAYER_NAME[4] = "Pl5Name";
			
			LBL_PLAYER_STATUS[0] = "Pl1State";
			LBL_PLAYER_STATUS[1] = "Pl2State";
			LBL_PLAYER_STATUS[2] = "Pl3State";
			LBL_PLAYER_STATUS[3] = "Pl4State";
			LBL_PLAYER_STATUS[4] = "Pl5State";
			
			LBL_CHAR_COUNT[0] = "StatusBarbarians";
			LBL_CHAR_COUNT[1] = "StatusArchers";
			LBL_CHAR_COUNT[2] = "StatusWizards";
			LBL_CHAR_COUNT[3] = "StatusWallBreakers";
			LBL_CHAR_COUNT[4] = "StatusHogRiders";
			LBL_CHAR_COUNT[5] = "StatusGiants";
			LBL_CHAR_COUNT[6] = "StatusDragons";
		}
		
	}
}