package com.utils;

import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class Outliner {
	public static Shape getOutline(Vector2f pos, float radius){
		
		pos = (pos == null)? new Vector2f(0,0):pos;
		
		Circle tempCircle = new Circle(pos.getX(), pos.getY(), radius);
		
			tempCircle.setCenterX(pos.getX());
			tempCircle.setCenterY(pos.getY());
		
		return tempCircle;
	}
	
	public static Shape getVisibleOutline(Shape outline, int mapX, int mapY){
		Circle tempCircle = new Circle(outline.getCenterX(), outline.getCenterY(), outline.getBoundingCircleRadius());
		
		tempCircle.setCenterX(tempCircle.getCenterX() + mapX);
		tempCircle.setCenterY(tempCircle.getCenterY() + mapY);
	
		return tempCircle;
	}
	
}
