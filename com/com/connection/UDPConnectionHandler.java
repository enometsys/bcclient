package com.connection;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;

import com.Main;
import com.packet.udp.UDPPacket;
import com.packet.udp.UDPPacketBuilder;

public class UDPConnectionHandler extends Thread {
	private static final String TAG = UDPConnectionHandler.class.getSimpleName();
	
	private UDPPacketBuilder	udpBuilder;
	private ConnectionInterface game;
	
	private InetAddress 		groupAddress;
	
	private DatagramSocket		datagramSocket;
	
	private MulticastSocket 	multicastSocket;
	private boolean 			listening;
	
	
	
	public UDPConnectionHandler(ConnectionInterface game, FailurReceiver failureReceiver, 
			InetAddress groupAddress, int groupPort, InetAddress serverAddress, int serverPort) {
		super();
		this.game = game;
		this.groupAddress = groupAddress;
		
		udpBuilder		= new UDPPacketBuilder(serverAddress, serverPort);
		
		try {
			Main.log.d(TAG, "UDP Multicast joining " + groupAddress + " on port " + groupPort);
			
			multicastSocket = new MulticastSocket(groupPort);
			multicastSocket.joinGroup(groupAddress);
			
			Main.log.d(TAG, "UDP initializing datagram socket...");
			
			datagramSocket = new DatagramSocket();
		} catch (IOException e) {
			//e.printStackTrace();
			Main.log.e(TAG, "Failed to start UDPConnection! " + e.getMessage());
			failureReceiver.failedToStartUDPConnection(e.getMessage());
		}
	}

	@Override
	public void run() {
		if (datagramSocket != null && multicastSocket != null){
			game.UDPConnected(this);
			
			startListening();
			
			game.UDPdisconnected();
		}
	}
	
	private synchronized boolean isListening(){
		return listening;
	}

	private void startListening() {
		listening = true;
		
		byte[] recvBuf = new byte[5000];
	    DatagramPacket packet;
	    
		while(isListening()){
			packet = new DatagramPacket(recvBuf,recvBuf.length);
			try {
				multicastSocket.receive(packet);
				processPacket(packet);
			} catch (IOException e) {
				Main.log.e(TAG, "Error receiving packet: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	private void processPacket(DatagramPacket dPckt){
		UDPPacket temp = UDPPacketBuilder.getUDPPacket(dPckt);		
		game.receiveUDPPacket(temp);
	}	

	public void sendPacket(UDPPacket outPacket) {
		Main.log.s(TAG, outPacket);
		
		DatagramPacket dPckt = udpBuilder.getUnicastDatagramPacket(outPacket);
		
		if (dPckt == null){
			Main.log.e(TAG, "Error creating datagram packet");
			return;
		}
		
		if (datagramSocket != null){
			try {
				datagramSocket.send(dPckt);
			} catch (IOException e) {}
		}
	}
	
	public void disconnect() {
		synchronized(this){
			listening = false;
		}
		
		try {
			multicastSocket.leaveGroup(groupAddress);
		} catch (IOException e) {}
		
		multicastSocket.close();
		datagramSocket.close();
	}
	
	
	public static interface ConnectionInterface{
		void UDPConnected(UDPConnectionHandler udpConnection);
		void sendUDPPacket(UDPPacket outPacket);
		void receiveUDPPacket(UDPPacket intPacket);
		void UDPdisconnected();
	}
	
	public static interface FailurReceiver{
		void failedToStartUDPConnection(String reason);
	}
}
