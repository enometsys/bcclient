package com.connection;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import com.Main;
import com.packet.tcp.TCPPacket;
import com.packet.tcp.TCPPacket.Scope;
import com.packet.tcp.TCPPacket.TCPPacketType;
import com.packet.tcp.response.Response;
import com.states.base.MatchState;
import com.utils.Util;

public class TCPConnectionHandler extends Thread{
	private static final String TAG = TCPConnectionHandler.class.getSimpleName();
	
	private ConnectionInterface menu, lobby, room, game;
	private Socket 				connectionSocket;
	
	private ObjectOutputStream 	out;
	private ObjectInputStream	in;
	
	private boolean				connected;
	
	public TCPConnectionHandler(String ipAddress, FailurReceiver failureReceiver, 
			ConnectionInterface menu, ConnectionInterface lobby, 
				ConnectionInterface room, ConnectionInterface game) {
		
		try {
			Main.log.d(TAG, "TCP Connecting to " + ipAddress + ":" + Util.TCP_PORT);
			connectionSocket = new Socket(ipAddress, Util.TCP_PORT);
			
			out = new ObjectOutputStream(connectionSocket.getOutputStream());
			in = new ObjectInputStream(connectionSocket.getInputStream());
			
			this.menu = menu;
			this.lobby = lobby;
			this.room = room;
			this.game = game;
			
		} catch (IOException e) {
			//e.printStackTrace();
			Main.log.e(TAG, "Failed to connect to server! " + e.getMessage());
			failureReceiver.failedToConnect(e.getMessage());
		}
	}

	@Override
	public void run() {
		if (connectionSocket != null){
			menu.TCPconnected(this);
			lobby.TCPconnected(this);
			room.TCPconnected(this);
			game.TCPconnected(this);
			
			startReceiving();
			
			menu.TCPdisconnected();
			lobby.TCPdisconnected();
			room.TCPdisconnected();
			game.TCPdisconnected();
		}
	}
	
	private synchronized boolean isConnected(){
		return connected;
	}
	
	private void startReceiving() {
		connected = true;
		while(isConnected()){
			try {
				processPacket((TCPPacket) in.readObject());
			} catch (ClassNotFoundException e) {
				Main.log.e(TAG, e.getMessage());
				e.printStackTrace();
			} catch (IOException e) {
			}
		}
	}

	private void processPacket(TCPPacket pckt){
		Main.log.r(TAG, pckt);		
		if (pckt.getScope() == Scope.GLOBAL){
			if (pckt.getTcpPacketType() == TCPPacketType.RESPONSE){
				switch(((Response)pckt).getReqType()){
					case JOIN_SERVER:
						menu.receiveTCPPacket(pckt);
						break;
					default:
						lobby.receiveTCPPacket(pckt);
						break;
				}
			}
		}else{
			if (MatchState.hasMatch()){
				if (MatchState.hasStarted()){
					game.receiveTCPPacket(pckt);
				}else{
					room.receiveTCPPacket(pckt);
				}
			}else{
				Main.log.w(TAG, "Receiveing Room scoped packets without a registered match");
			}
		}
	}

	public void sendPacket(TCPPacket outPacket) {
		Main.log.s(TAG, outPacket);
		if (connectionSocket != null){
			try {
				out.writeObject(outPacket);
			} catch (IOException e) {}
		}
	}
	
	public void disconnect() {
		synchronized(this){
			connected = false;
		}
		
		try {
			connectionSocket.close();
		} catch (IOException e) {}
	}
	
	public static interface ConnectionInterface{
		void TCPconnected(TCPConnectionHandler tcpConnection);
		void sendTCPPacket(TCPPacket outPacket);
		void receiveTCPPacket(TCPPacket intPacket);
		void TCPdisconnected();
	}
	
	public static interface FailurReceiver{
		void failedToConnect(String reason);
	}
}
