package com;

public class Player {
	private final int		playerId;
	private final String 	playerName;
	private JoinedMatch 	joinedMatch;
	
	public Player(int playerId, String playerName, JoinedMatch joinedMatch) {
		super();
		this.playerId 		= playerId;
		this.playerName 	= playerName;
		this.joinedMatch 	= joinedMatch;
	}
	
	public Player(int playerId, String playerName) {
		this(playerId, playerName, null);
	}

	public int getPlayerId() {
		return playerId;
	}

	public String getPlayerName() {
		return playerName;
	}
	
	public boolean hasMatch(){
		return joinedMatch != null;
	}
	
	public void joinMatch(JoinedMatch joinedMatch){
		this.joinedMatch = joinedMatch;
	}
	
	public void leaveMatch(){
		this.joinedMatch = null;
	}
	
	public boolean isHost(){
		if (!hasMatch()) return false;
		return joinedMatch.getHostId() == playerId;
	}
	
	public boolean isMe(MatchPlayer otherPlayer){
		return isSame(otherPlayer);
	}
	
	public boolean isSame(Player otherPlayer){
		return isSame(otherPlayer.getPlayerId(), otherPlayer.getPlayerName());
	}
	
	public boolean isMe(int playerId, String playerName){
		return isSame(playerId, playerName);
	}
	
	public boolean isMe(int playerId){
		return this.playerId == playerId;
	}
	
	public boolean isSame(int playerId, String playerName){
		return (this.playerId==playerId) && (this.playerName.equalsIgnoreCase(playerName));
	}
	

	
	
	
	public static final class MatchPlayer extends Player{
		private int		mapLocation;
		private boolean	ready;
		private boolean	initialized;
		
		public MatchPlayer(int playerId, String playerName, int mapLocation, boolean ready, JoinedMatch joinedMatch) {
			super(playerId, playerName, joinedMatch);
			this.mapLocation = mapLocation;
			this.ready = ready;
		}

		public int getMapLocation() {
			return mapLocation;
		}

		public boolean isReady() {
			return ready;
		}

		public boolean isInitialized() {
			return initialized;
		}
		
		public void initializePlayer(){
			this.initialized = true;
		}
		
		public boolean update(int mapLocation, boolean ready){
			Boolean updated = this.mapLocation != mapLocation || this.ready != ready;
			
			this.mapLocation = mapLocation;
			this.ready = ready;
			
			return updated;
		}
	}
}
