package com;

import java.net.InetAddress;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.ScalableGame;
import org.newdawn.slick.SlickException;

import com.connection.TCPConnectionHandler;
import com.connection.TCPConnectionHandler.FailurReceiver;
import com.packet.Packet;
import com.connection.UDPConnectionHandler;
import com.states.Game;
import com.states.Lobby;
import com.states.Menu;
import com.states.Room;
import com.states.StateController;
import com.utils.Logger;
import com.utils.Util;

public final class Main implements Menu.TCPConnector, Game.UDPConnector{
	public static final boolean 		test = Packet.test;
	public static final boolean 		debug = true;
	public static boolean 				autoLogin = false;
	
	private static final String TAG = Main.class.getSimpleName();
	public static Logger log;
	
	private static TCPConnectionHandler tcpConnectionHandler; 
	private static UDPConnectionHandler udpConnectionHandler; 
	
	private static AppGameContainer 	appgc;	
	private Menu 					menu;
	private Lobby 					lobby;
	private Room 					room;
	private Game 					game;
	
	public Main() {
		super();
		
		log = new Logger();
		
		this.menu = new Menu(this);
		this.lobby = new Lobby();
		this.room = new Room(this);
		this.game = new Game(this);
		
		try {			
			ScalableGame temp = new ScalableGame(
					new StateController(Util.GAME_NAME, menu, lobby, room, game), 
					Util.X_SIZE, Util.Y_SIZE);
			
			appgc = new AppGameContainer(temp);
			appgc.setDisplayMode(Util.X_SIZE, Util.Y_SIZE, false);
			appgc.setTargetFrameRate(Util.PREF_FPS);
			appgc.setShowFPS(false);
			appgc.setVerbose(false);
			
			appgc.start();			
			
		} catch (SlickException e) {
			log.e(TAG, e.getMessage());
		}
	}

	@Override
	public void connectToServer(String ipAddress, FailurReceiver failureReceiver) {
		tcpConnectionHandler = new TCPConnectionHandler(ipAddress, failureReceiver, menu, lobby, room, game);
		tcpConnectionHandler.start();
	}
	
	@Override
	public void startUDPConnection(InetAddress groupAddress, int groupPort, InetAddress serverAddress, int serverPort) {
		udpConnectionHandler = new UDPConnectionHandler(game, game, groupAddress, groupPort, serverAddress, serverPort);
		udpConnectionHandler.start();
	}
	
	public static void disconnect(){
		if (tcpConnectionHandler != null)
			tcpConnectionHandler.disconnect();
		
		if (udpConnectionHandler != null)
			udpConnectionHandler.disconnect();
	}
	
	public static void exitGame() {
		disconnect();
		appgc.exit();
		System.exit(0);
	}
}
