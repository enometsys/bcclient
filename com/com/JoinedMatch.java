package com;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.Player.MatchPlayer;

public final class JoinedMatch extends Match{
	public JoinedMatch(int matchId, String matchName, int mapId, int hostId) {
		super(matchId, matchName, mapId, hostId);
		players = new HashMap<Integer, MatchPlayer>();
		this.started = false;
	}

	private boolean started;
	private Map<Integer, MatchPlayer> players;

	public boolean isStarted() {
		return started;
	}

	public boolean updated(int mapId, int hostId) {
		Boolean updated = super.mapId != mapId || super.hostId != hostId; 
		
		super.mapId = mapId;
		super.hostId = hostId;
		
		return updated;
	}

	public void startMatch(){
		started = true;
	}
	
	public MatchPlayer getPlayer(int playerId){
		if (players.containsKey(playerId)){
			return players.get(playerId);
		}
		
		return null;
	}
	
	public Collection<MatchPlayer> getPlayers(){
		return players.values();
	}
	
	public Collection<MatchPlayer> getInitializedPlayers(){
		ArrayList<MatchPlayer> temp = new ArrayList<MatchPlayer>();
		for(MatchPlayer pl:getPlayers()){
			if (pl.isInitialized()) temp.add(pl);
		}
		
		return temp;
	}

	public boolean playerUpdated(int playerId, String playerName, int mapLocation, boolean left, boolean ready) {
		if (players.containsKey(playerId)){
			
			if (left){
				players.remove(playerId);
				return true;
			}
			
			return players.get(playerId).update(mapLocation, ready);
		}
		
		if (!left){
			players.put(playerId, new MatchPlayer(playerId, playerName, mapLocation, ready, this));
			return true;
		}
		
		return false;
	}
	
	public boolean canStart() {
		return !isStarted() && (getPlayers().size() > 1) && everyoneReady();
	}

	private boolean everyoneReady() {
		for(MatchPlayer pl: getPlayers()){
			if (!pl.isReady()) return false;
		}
		
		return true;
	}

	public boolean isAllInitialized() {
		for(MatchPlayer pl:getPlayers()){
			if (!pl.isInitialized()) return false;
		}
		
		return true;
	}

	public Collection<MatchPlayer> getUnInitializedPlayers() {
		ArrayList<MatchPlayer> temp = new ArrayList<MatchPlayer>();
		for(MatchPlayer pl:getPlayers()){
			if (!pl.isInitialized()) temp.add(pl);
		}
		
		return temp;
	}
}