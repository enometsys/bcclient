package com;

import com.packet.tcp.response.MatchDetails.List;

public abstract class Match {
	protected final int 	matchId;
	protected final String 	matchName;
	protected int			mapId;
	protected int			hostId;
	
	public Match(int matchId, String matchName, int mapId, int hostId) {
		super();
		this.matchId = matchId;
		this.matchName = matchName;
		this.mapId = mapId;
		this.hostId = hostId;
	}

	public int getMatchId() {
		return matchId;
	}

	public String getMatchName() {
		return matchName;
	}

	public int getMapId() {
		return mapId;
	}

	public int getHostId() {
		return hostId;
	}
	
	public boolean isSame(int matchId, String matchName) {
		return (this.matchId == matchId) && (this.matchName.equalsIgnoreCase(matchName));
	}
	
	public static final class ListMatch extends Match{
		private final String 	hostName;
		private final int		numOfPlayers;
		
		public ListMatch(int matchId, String matchName, int mapId, int hostId, String hostName, int numOfPlayers) {
			super(matchId, matchName, mapId, hostId);
			this.hostName = hostName;
			this.numOfPlayers = numOfPlayers;
		}

		public ListMatch(List matchDetails) {
			this(matchDetails.getMatchId(), matchDetails.getMatchName(), matchDetails.getMapId()
					, matchDetails.getHostId(), matchDetails.getHostName(), matchDetails.getNumOfPlayers());
		}

		public String getHostName() {
			return hostName;
		}

		public int getNumOfPlayers() {
			return numOfPlayers;
		}

		@Override
		public String toString() {
			return 	"                " + 		
					getMatchId() 	+ "                                                     " + 
					getMatchName() 	+ "                                                              " + 
					getHostName() 	+ "                                                     " + 
					getNumOfPlayers() + " / 5";
		}
		
		
	}
}
