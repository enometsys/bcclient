package com.objects.gamecharacter;

import org.newdawn.slick.geom.Vector2f;

import com.objects.mover.Mover;
import com.objects.utils.BarracksDetails;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;

public class GameCharacter extends Mover{

	//From update
	public GameCharacter(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		super(playerId, objectId, objectType, gameConnection, update);
	}

	//From barracks
	public GameCharacter(int playerId, int objectId, int objectType, NewGameConnection gameConnection,
			Vector2f position) {
		super(playerId, objectId, objectType, gameConnection, position);
	}

	@Override
	protected void deactivate() {
		super.deactivate();
		BarracksDetails.gameCharacterDied(playerId, objectType);
	}
}
