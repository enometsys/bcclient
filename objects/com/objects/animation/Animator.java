package com.objects.animation;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;

import com.Main;
import com.objects.GameObject.State;
import com.objects.utils.AttackerDetails;
import com.objects.utils.MoverDetails;
import com.objects.utils.ObjectDetails;
import com.objects.utils.ObjectDetails.K;

public class Animator {

private static final String TAG = Animator.class.getSimpleName();
	public static Animation getAnimation(int mapLocation, int objectTypeId){
		if (ObjectDetails.isObject(objectTypeId)){
			Map<State, SpriteSheet> mapper = new HashMap<State, SpriteSheet>();
			SpriteSheet mapperSheet = null;	
			
			int playerColor = (ObjectDetails.isGameCharacter(objectTypeId))? mapLocation:0;
								
			int x_size = ANIM.SPRITE_SIZE_X;
			int y_size = ANIM.SPRITE_SIZE_Y;
			
			switch(objectTypeId){
				case K.unit_hogrider		:
				case K.unit_dragon 			:
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	:
				case K.unit_giant 			:
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	:
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			:
				
				case K.build_town_hall		:
				case K.build_wall			:
				case K.build_bar_wizard		:
				case K.build_bar_hog		:
				case K.build_bar_giant		:
				case K.build_bar_dragon		: x_size = ANIM.SPRITE_SIZE_X; y_size = ANIM.SPRITE_SIZE_Y;
			}
			
			try {
				
				if (MoverDetails.isMover(objectTypeId)){
					//MOVE
					mapperSheet = new SpriteSheet(ANIM.PATH_ANIM_MOVE[objectTypeId][playerColor], x_size, y_size);
					mapper.put(State.MOVE, mapperSheet);
				}
				
				if (AttackerDetails.isAttacker(objectTypeId)){
					//ATTACK
					mapperSheet = new SpriteSheet(ANIM.PATH_ANIM_ATTACK[objectTypeId][playerColor], x_size, y_size);
					mapper.put(State.ATTACK, mapperSheet);
				}

				//IDLE25
				mapperSheet = new SpriteSheet(ANIM.PATH_ANIM_IDLE25[objectTypeId][playerColor], x_size, y_size);
				mapper.put(State.IDLE_25, mapperSheet);		
				
				//IDLE75
				mapperSheet = new SpriteSheet(ANIM.PATH_ANIM_IDLE75[objectTypeId][playerColor], x_size, y_size);
				mapper.put(State.IDLE_75, mapperSheet);
				
				//DESTROYED
				mapperSheet = new SpriteSheet(ANIM.PATH_ANIM_DESTROYED[objectTypeId][playerColor], x_size, y_size);
				mapper.put(State.DESTROYED, mapperSheet);
				
				return new Animation(mapper);
				
			} catch (SlickException e) {
				Main.log.e(TAG, "Error on creating animation: " + e.getMessage());
				e.printStackTrace();
			}
			
		}
		
		Main.log.e(TAG, "Error creating NewAnimation");
		return null;
	}
	
	public static final class ANIM{
		public static final int SPRITE_ROW = 3; 
		public static final int SPRITE_SIZE_X = 32;
		public static final int SPRITE_SIZE_Y = 32;
		public static final float SPRITE_SCALE = 1.0f; 
		public static final int SPRITE_DURATION = 200;
		
		public static final int dir_up 		= 3;
		public static final int dir_left 	= 1;
		public static final int dir_right 	= 2;
		public static final int dir_down 	= 0;
		
		public static final int player_me 		= 0;
		public static final int player_other1 	= 1;
		public static final int player_other2 	= 2;
		public static final int player_other3 	= 3;
		public static final int player_other4 	= 4;
		
		public static final String[][] PATH_ANIM_JUMP		= new String[2][];
		public static final String[][] PATH_ANIM_MOVE		= new String[12][];
		public static final String[][] PATH_ANIM_ATTACK		= new String[17][];
		public static final String[][] PATH_ANIM_IDLE75		= new String[22][];
		public static final String[][] PATH_ANIM_IDLE25		= new String[22][];
		public static final String[][] PATH_ANIM_DESTROYED	= new String[22][];
		
		static{
			for(int i=0; i< 2; i++)  PATH_ANIM_JUMP[i] = new String[5];
			for(int i=0; i< 12; i++) PATH_ANIM_MOVE[i] = new String[5];
			for(int i=0; i< 17; i++) PATH_ANIM_ATTACK[i] = new String[5];
			for(int i=0; i< 22; i++) PATH_ANIM_IDLE75[i] = new String[5];
			for(int i=0; i< 22; i++) PATH_ANIM_IDLE25[i] = new String[5];
			for(int i=0; i< 22; i++) PATH_ANIM_DESTROYED[i] = new String[5];
			
			for(int i = 0; i < 5 ; i++){
				/**
				 * JUMPING NewAnimation
				 */
				PATH_ANIM_JUMP[K.unit_hogrider][i] 		= "res/spritesheets/mobile-units/" + i + "/hogrider/jump.png";
				PATH_ANIM_JUMP[K.unit_dragon][i] 			= "res/spritesheets/mobile-units/" + i + "/dragon/jump.png";
		
				
				/**
				 * MOVING NewAnimation
				 */
				PATH_ANIM_MOVE[K.unit_hogrider][i] 		= "res/spritesheets/mobile-units/" + i + "/hogrider/move.png";
				PATH_ANIM_MOVE[K.unit_archer][i] 			= "res/spritesheets/mobile-units/" + i + "/archer/move.png";
				PATH_ANIM_MOVE[K.unit_barbarian][i] 		= "res/spritesheets/mobile-units/" + i + "/barbarian/move.png";
				PATH_ANIM_MOVE[K.unit_wizard][i] 			= "res/spritesheets/mobile-units/" + i + "/wizard/move.png";
				PATH_ANIM_MOVE[K.unit_wallbreaker][i]		= "res/spritesheets/mobile-units/" + i + "/wallbreaker/move.png";
				PATH_ANIM_MOVE[K.unit_giant][i] 			= "res/spritesheets/mobile-units/" + i + "/giant/move.png";
				PATH_ANIM_MOVE[K.unit_dragon][i] 			= "res/spritesheets/mobile-units/" + i + "/dragon/move.png";
				
				/**
				 * ATTACKING NewAnimation
				 */
				PATH_ANIM_ATTACK[K.unit_hogrider][i] 		= "res/spritesheets/mobile-units/" + i + "/hogrider/attack.png";
				PATH_ANIM_ATTACK[K.unit_archer][i] 		= "res/spritesheets/mobile-units/" + i + "/archer/attack.png";
				PATH_ANIM_ATTACK[K.unit_barbarian][i] 	= "res/spritesheets/mobile-units/" + i + "/barbarian/attack.png";
				PATH_ANIM_ATTACK[K.unit_wizard][i] 		= "res/spritesheets/mobile-units/" + i + "/wizard/attack.png";
				PATH_ANIM_ATTACK[K.unit_wallbreaker][i] 	= "res/spritesheets/mobile-units/" + i + "/wallbreaker/attack.png";
				PATH_ANIM_ATTACK[K.unit_giant][i] 		= "res/spritesheets/mobile-units/" + i + "/giant/attack.png";
				PATH_ANIM_ATTACK[K.unit_dragon][i] 		= "res/spritesheets/mobile-units/" + i + "/dragon/attack.png";
				
				/**
				 * IDLE with greater than 75% life NewAnimation
				 */
				PATH_ANIM_IDLE75[K.unit_hogrider][i]	 	= "res/spritesheets/mobile-units/" + i + "/hogrider/idle75.png";
				PATH_ANIM_IDLE75[K.unit_archer][i] 		= "res/spritesheets/mobile-units/" + i + "/archer/idle75.png";
				PATH_ANIM_IDLE75[K.unit_barbarian][i] 	= "res/spritesheets/mobile-units/" + i + "/barbarian/idle75.png";
				PATH_ANIM_IDLE75[K.unit_wizard][i] 		= "res/spritesheets/mobile-units/" + i + "/wizard/idle75.png";
				PATH_ANIM_IDLE75[K.unit_wallbreaker][i] 	= "res/spritesheets/mobile-units/" + i + "/wallbreaker/idle75.png";
				PATH_ANIM_IDLE75[K.unit_giant][i] 		= "res/spritesheets/mobile-units/" + i + "/giant/idle75.png";
				PATH_ANIM_IDLE75[K.unit_dragon][i] 		= "res/spritesheets/mobile-units/" + i + "/dragon/idle75.png";
				
				/**
				 * IDLE with greater than 25% life NewAnimation
				 */
				PATH_ANIM_IDLE25[K.unit_hogrider][i] 		= "res/spritesheets/mobile-units/" + i + "/hogrider/idle25.png";
				PATH_ANIM_IDLE25[K.unit_archer][i] 		= "res/spritesheets/mobile-units/" + i + "/archer/idle25.png";
				PATH_ANIM_IDLE25[K.unit_barbarian][i] 	= "res/spritesheets/mobile-units/" + i + "/barbarian/idle25.png";
				PATH_ANIM_IDLE25[K.unit_wizard][i] 		= "res/spritesheets/mobile-units/" + i + "/wizard/idle25.png";
				PATH_ANIM_IDLE25[K.unit_wallbreaker][i] 	= "res/spritesheets/mobile-units/" + i + "/wallbreaker/idle25.png";
				PATH_ANIM_IDLE25[K.unit_giant][i] 		= "res/spritesheets/mobile-units/" + i + "/giant/idle25.png";
				PATH_ANIM_IDLE25[K.unit_dragon][i] 		= "res/spritesheets/mobile-units/" + i + "/dragon/idle25.png";
				
				/**
				 * DESTROYED NewAnimation
				 */
				PATH_ANIM_DESTROYED[K.unit_hogrider][i] 	= "res/spritesheets/mobile-units/" + i + "/hogrider/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_archer][i] 	= "res/spritesheets/mobile-units/" + i + "/archer/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_barbarian][i]	= "res/spritesheets/mobile-units/" + i + "/barbarian/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_wizard][i] 	= "res/spritesheets/mobile-units/" + i + "/wizard/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_wallbreaker][i]= "res/spritesheets/mobile-units/" + i + "/wallbreaker/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_giant][i] 		= "res/spritesheets/mobile-units/" + i + "/giant/destroy.png";
				PATH_ANIM_DESTROYED[K.unit_dragon][i] 	= "res/spritesheets/mobile-units/" + i + "/dragon/destroy.png";
			}
			
			PATH_ANIM_MOVE[K.proj_arrow][0] 		= "res/spritesheets/projectile/arrow/move.png";
			PATH_ANIM_MOVE[K.proj_cannon_ball][0]= "res/spritesheets/projectile/cannonball/move.png";
			PATH_ANIM_MOVE[K.proj_fire_breath][0]= "res/spritesheets/projectile/firebreath/move.png";
			PATH_ANIM_MOVE[K.proj_magic_attack][0] = "res/spritesheets/projectile/magic-attack/move.png";
			PATH_ANIM_MOVE[K.proj_mortar_blast][0] = "res/spritesheets/projectile/mortar/move.png";
			
			/**
			 * ATTACK NewAnimation
			 */
			PATH_ANIM_ATTACK[K.tow_archer][0] 		= "res/spritesheets/towers/archer-tower/attack.png";
			PATH_ANIM_ATTACK[K.tow_cannon][0] 		= "res/spritesheets/towers/cannon/attack.png";
			PATH_ANIM_ATTACK[K.tow_mortar][0] 		= "res/spritesheets/towers/mortar/attack.png";
			PATH_ANIM_ATTACK[K.tow_wizard][0]	 		= "res/spritesheets/towers/wizard-tower/attack.png";
			
			PATH_ANIM_ATTACK[K.proj_arrow][0] 		= "res/spritesheets/projectile/arrow/attack.png";
			PATH_ANIM_ATTACK[K.proj_cannon_ball][0]= "res/spritesheets/projectile/cannonball/attack.png";
			PATH_ANIM_ATTACK[K.proj_fire_breath][0]= "res/spritesheets/projectile/firebreath/attack.png";
			PATH_ANIM_ATTACK[K.proj_magic_attack][0] = "res/spritesheets/projectile/magic-attack/attack.png";
			PATH_ANIM_ATTACK[K.proj_mortar_blast][0] = "res/spritesheets/projectile/mortar/attack.png";
			
			/**
			 * IDLE with greater than 75% life NewAnimation
			 */
			PATH_ANIM_IDLE75[K.tow_archer][0] 		= "res/spritesheets/towers/archer-tower/idle75.png";;
			PATH_ANIM_IDLE75[K.tow_cannon][0] 		= "res/spritesheets/towers/cannon/idle75.png";
			PATH_ANIM_IDLE75[K.tow_mortar][0] 		= "res/spritesheets/towers/mortar/idle75.png";
			PATH_ANIM_IDLE75[K.tow_wizard][0] 		= "res/spritesheets/towers/wizard-tower/idle75.png";
			
			
			PATH_ANIM_IDLE75[K.build_town_hall][0] 	= "res/spritesheets/town-hall/idle75.png";
			PATH_ANIM_IDLE75[K.build_wall][0] 		= "res/spritesheets/wall/idle75.png";
			PATH_ANIM_IDLE75[K.build_bar_wizard][0] 	= "res/spritesheets/barracks/wizard-barracks/idle75.png";
			PATH_ANIM_IDLE75[K.build_bar_hog][0] 		= "res/spritesheets/barracks/hogrider-barracks/idle75.png";
			PATH_ANIM_IDLE75[K.build_bar_giant][0]	= "res/spritesheets/barracks/giant-barracks/idle75.png";
			PATH_ANIM_IDLE75[K.build_bar_dragon][0] 	= "res/spritesheets/barracks/dragon-barracks/idle75.png";
			
			PATH_ANIM_IDLE75[K.proj_arrow][0] 		= "res/spritesheets/projectile/arrow/idle75.png";
			PATH_ANIM_IDLE75[K.proj_cannon_ball][0]	= "res/spritesheets/projectile/cannonball/idle75.png";
			PATH_ANIM_IDLE75[K.proj_fire_breath][0]	= "res/spritesheets/projectile/firebreath/idle75.png";
			PATH_ANIM_IDLE75[K.proj_magic_attack][0] 	= "res/spritesheets/projectile/magic-attack/idle75.png";
			PATH_ANIM_IDLE75[K.proj_mortar_blast][0] 	= "res/spritesheets/projectile/mortar/idle75.png";
			
			/**
			 * IDLE with greater than 25% life NewAnimation
			 */			
			PATH_ANIM_IDLE25[K.tow_archer][0] 		= "res/spritesheets/towers/archer-tower/idle25.png";;
			PATH_ANIM_IDLE25[K.tow_cannon][0] 		= "res/spritesheets/towers/cannon/idle25.png";
			PATH_ANIM_IDLE25[K.tow_mortar][0]			= "res/spritesheets/towers/mortar/idle25.png";
			PATH_ANIM_IDLE25[K.tow_wizard][0] 		= "res/spritesheets/towers/wizard-tower/idle25.png";
			
			PATH_ANIM_IDLE25[K.build_town_hall][0] 	= "res/spritesheets/town-hall/idle25.png";
			PATH_ANIM_IDLE25[K.build_wall][0] 		= "res/spritesheets/wall/idle25.png";
			PATH_ANIM_IDLE25[K.build_bar_wizard][0] 	= "res/spritesheets/barracks/wizard-barracks/idle25.png";
			PATH_ANIM_IDLE25[K.build_bar_hog][0] 		= "res/spritesheets/barracks/hogrider-barracks/idle25.png";
			PATH_ANIM_IDLE25[K.build_bar_giant][0] 	= "res/spritesheets/barracks/giant-barracks/idle25.png";
			PATH_ANIM_IDLE25[K.build_bar_dragon][0] 	= "res/spritesheets/barracks/dragon-barracks/idle25.png";
			
			PATH_ANIM_IDLE25[K.proj_arrow][0] 		= "res/spritesheets/projectile/arrow/idle25.png";
			PATH_ANIM_IDLE25[K.proj_cannon_ball][0]= "res/spritesheets/projectile/cannonball/idle25.png";
			PATH_ANIM_IDLE25[K.proj_fire_breath][0]= "res/spritesheets/projectile/firebreath/idle25.png";
			PATH_ANIM_IDLE25[K.proj_magic_attack][0] = "res/spritesheets/projectile/magic-attack/idle25.png";
			PATH_ANIM_IDLE25[K.proj_mortar_blast][0] = "res/spritesheets/projectile/mortar/idle25.png";
			
			/**
			 * DESTROYED NewAnimation
			 */
			PATH_ANIM_DESTROYED[K.tow_archer][0] 		= "res/spritesheets/towers/archer-tower/destroy.png";;
			PATH_ANIM_DESTROYED[K.tow_cannon][0] 		= "res/spritesheets/towers/cannon/destroy.png";
			PATH_ANIM_DESTROYED[K.tow_mortar][0] 		= "res/spritesheets/towers/mortar/destroy.png";
			PATH_ANIM_DESTROYED[K.tow_wizard][0] 		= "res/spritesheets/towers/wizard-tower/destroy.png";
			
			PATH_ANIM_DESTROYED[K.build_town_hall][0] = "res/spritesheets/town-hall/destroy.png";
			PATH_ANIM_DESTROYED[K.build_wall][0] 		= "res/spritesheets/wall/destroy.png";
			PATH_ANIM_DESTROYED[K.build_bar_wizard][0]= "res/spritesheets/barracks/wizard-barracks/destroy.png";
			PATH_ANIM_DESTROYED[K.build_bar_hog][0] 	= "res/spritesheets/barracks/hogrider-barracks/destroy.png";
			PATH_ANIM_DESTROYED[K.build_bar_giant][0] = "res/spritesheets/barracks/giant-barracks/destroy.png";
			PATH_ANIM_DESTROYED[K.build_bar_dragon][0]= "res/spritesheets/barracks/dragon-barracks/destroy.png";
			
			PATH_ANIM_DESTROYED[K.proj_arrow][0] 		= "res/spritesheets/projectile/arrow/destroy.png";
			PATH_ANIM_DESTROYED[K.proj_cannon_ball][0]= "res/spritesheets/projectile/cannonball/destroy.png";
			PATH_ANIM_DESTROYED[K.proj_fire_breath][0]= "res/spritesheets/projectile/firebreath/destroy.png";
			PATH_ANIM_DESTROYED[K.proj_magic_attack][0] = "res/spritesheets/projectile/magic-attack/destroy.png";
			PATH_ANIM_DESTROYED[K.proj_mortar_blast][0] = "res/spritesheets/projectile/mortar/destroy.png";
		}
	}

}
