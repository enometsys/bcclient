package com.objects.animation;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.SpriteSheet;

import com.objects.animation.Animator.ANIM;
import com.objects.GameObject;
import com.objects.GameObject.Orientation;
import com.objects.GameObject.State;

public class Animation {

protected Map<State, Map<Orientation, org.newdawn.slick.Animation>> animation;
	
	public Animation(Map<State, SpriteSheet> input) {
		animation = new HashMap<State, Map<Orientation, org.newdawn.slick.Animation>>();
		
		for(State s: input.keySet()){
			
			org.newdawn.slick.Animation tempUp = new org.newdawn.slick.Animation();
			org.newdawn.slick.Animation tempLeft = new org.newdawn.slick.Animation();
			org.newdawn.slick.Animation tempRight = new org.newdawn.slick.Animation();
			org.newdawn.slick.Animation tempDown = new org.newdawn.slick.Animation();
			
			for(int i = 0; i < ANIM.SPRITE_ROW; i++){
				//Add facing up animation
				tempUp.addFrame(input.get(s).getSubImage(i, 
						ANIM.dir_up).getScaledCopy(ANIM.SPRITE_SCALE), ANIM.SPRITE_DURATION);
				
				//Add facing left animation
				tempLeft.addFrame(input.get(s).getSubImage(i, 
						ANIM.dir_left).getScaledCopy(ANIM.SPRITE_SCALE), ANIM.SPRITE_DURATION);
				
				//Add facing right animation
				tempRight.addFrame(input.get(s).getSubImage(i, 
						ANIM.dir_right).getScaledCopy(ANIM.SPRITE_SCALE), ANIM.SPRITE_DURATION);
				
				//Add facing down animation
				tempDown.addFrame(input.get(s).getSubImage(i, 
						ANIM.dir_down).getScaledCopy(ANIM.SPRITE_SCALE), ANIM.SPRITE_DURATION);
			}
			
			Map<Orientation, org.newdawn.slick.Animation> tempAnim = new HashMap<Orientation, org.newdawn.slick.Animation>();
			
			tempAnim.put(Orientation.UP, tempUp);
			tempAnim.put(Orientation.LEFT, tempLeft);
			tempAnim.put(Orientation.RIGHT, tempRight);
			tempAnim.put(Orientation.DOWN, tempDown);
			
			animation.put(s, tempAnim);
		}
	}
	
	public void draw(GameObject go, int mapX, int mapY) {
		animation.get(	go.getState()).get( go.getOrientation()).draw(
							go.getPosition().getX() + mapX - 16, 
							go.getPosition().getY() + mapY - 16		
						);
	}
	
	public boolean finishedAnimatingDeathScene(){
		return true;
	}

}
