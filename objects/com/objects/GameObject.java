package com.objects;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.objects.animation.Animation;
import com.objects.animation.Animator;
import com.objects.utils.ObjectDetails;
import com.packet.udp.Action;
import com.packet.udp.UDPPacketBuilder;
import com.states.base.BaseState;
import com.states.base.MatchState;
import com.states.game.NewGameConnection;
import com.utils.Outliner;

public class GameObject {
	protected final int						playerId;
	protected final int						objectId;
	protected final int						objectType;
	protected final NewGameConnection 		gameConnection;
	protected final	boolean					enemy;
	
	private Animation 				animation;
	private final double 					maxLifePoints;
	
	private boolean						isControlled;
	private double						lifePoints;
	private Shape						outline;
	private Vector2f					position;
	
	private boolean						active;
	private boolean 					alive;
	private Orientation					orientation;
	private State						state;
	
	private int							updateInterval;
	private int							updateDelta;
	
	private boolean						selected;
	private Action 						update;
	
	private int 						mapLoc;	
	private boolean						obstacleAdded;
		
	protected GameObject(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position, Action update) {
		super();
		
		this.playerId 		= playerId;
		this.objectId 		= objectId;
		this.objectType 	= objectType;
		this.gameConnection = gameConnection;
		
		this.maxLifePoints 	= ObjectDetails.getMaxLifePoints(objectType);
		this.enemy 			= !BaseState.getPlayer().isMe(playerId);
		
		this.mapLoc 		= (MatchState.getSJoinedMatch().getPlayer(playerId) != null)? MatchState.getSJoinedMatch().getPlayer(playerId).getMapLocation():0;
		
		this.animation 		= null;
		this.isControlled 	= this.enemy;
		this.lifePoints 	= this.maxLifePoints;
		this.position 		= (position == null)? new Vector2f(update.getPosX(), update.getPosY()):position;
		this.outline 		= ObjectDetails.getOutline(position, objectType);
		this.active 		= true;
		this.alive			= true;
		this.orientation 	= Orientation.DOWN;
		this.selected 		= false;
		
		this.updateInterval	= 1000;
		this.updateDelta	= 1000;
		
		this.update 		= update;
				
		setIdle();
		
		if (gameConnection.getObstaclesManager() != null){
			gameConnection.getObstaclesManager().addToObstacles(this);
			this.obstacleAdded = true;
		}
		
		sendUpdate();
	}
	
	//Without Update
	public GameObject(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position) {
		this(playerId, objectId, objectType, gameConnection, position, null);
	}
	
	//With update
	public GameObject(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		this(playerId, objectId, objectType, gameConnection, null, update);
	}
	
	/**
	 * ====================================== SIMPLE GETTERS ===========================================
	 */
	public Vector2f getPosition() {
		return position;
	}

	public boolean isActive() {
		return active;
	}

	public int getPlayerId() {
		return playerId;
	}

	public int getObjectId() {
		return objectId;
	}
	
	public int getObjectType(){
		return objectType;
	}

	public void selected(boolean selected) {
		this.selected = selected;
	}

	public Shape getOutline() {
		return outline;
	}
	
	public boolean isEnemy() {
		return enemy;
	}	
	
	public boolean isControlled() {
		return isControlled;
	}

	public boolean isAlive() {
		return alive;
	}

	public Orientation getOrientation() {
		return orientation;
	}

	public State getState() {
		return state;
	}
	
	public NewGameConnection getGameConnection() {
		return gameConnection;
	}

	/**
	 * ==================================== SETTERS AND LIFE FUNCTIONS ===========================================
	 */
	protected void move(Vector2f newPosition){
		position = newPosition;
		outline.setCenterX(position.x);
		outline.setCenterY(position.y);
	}
	
	protected final void setIdle() {
		setState(State.IDLE_75);
		if (lifePoints <= (maxLifePoints * 0.25))
			setState(State.IDLE_25);
	}
	
	protected final void setState(State state) {
		this.state = state;
	}
	
	protected final void setOrientation(Orientation orientation) {
		this.orientation = orientation;
	}
	
	protected boolean takeDamage(double damage, GameObject attacker){
		
		lifePoints -= damage;
		
		if (lifePoints <= 0){
			die(attacker);
			return false;
		}
			
		sendUpdate();
		return true;
	}
	
	protected void die(GameObject attacker){
		alive = false;
		lifePoints = 0;
		state = State.DESTROYED;
		
		if (objectId == ObjectDetails.K.build_town_hall){ 
			if (attacker != null)
				sendGameOverUpdate(attacker.getPlayerId());
		}else sendDestroyedUpdate();
	}

	protected void deactivate(){
		active = false;
		gameConnection.getObstaclesManager().removeFromObstacles(this);
	}
	
	public final void attack(double damage, GameObject attacker){
		takeDamage(damage, attacker);
	}
	
	/**
	 * ===================================	SLICK2D FUNCTIONS ============================================
	 */
	public final void renderObject(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY,
			boolean renderObjectGeo) {
		if (!isActive()) return;
		
		if (animation != null){
			animation.draw(this, mapX, mapY);
		}
		
		if (state == State.DESTROYED){
			if (animation != null){
				if (animation.finishedAnimatingDeathScene()){
					deactivate();
					return;
				}
			}else{
				deactivate();
				return;
			}
		}
		
		if (selected){	//for selection mark
			g.setColor(Color.green);
			g.draw(Outliner.getVisibleOutline(getOutline(), mapX, mapY));
		}
		
		if (renderObjectGeo) renderDebug(gc, sbg, g, mapX, mapY, 50, 150);
	}
	
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY, int xLoc, int yLoc){
		return yLoc;
	}
	
	public synchronized final void updateObject(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		if (!isActive() || !isAlive()) return;
		if (animation == null) this.animation = Animator.getAnimation(mapLoc, objectType);;
		if (!this.obstacleAdded) {
			if (gameConnection.getObstaclesManager() != null){
				gameConnection.getObstaclesManager().addToObstacles(this);
				this.obstacleAdded = true;
			}else return;
		}
		
		updateDelta(delta);
		updateGameUnp(gc, sbg, delta, mapX, mapY);
	}

	protected void updateGameUnp(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		// TODO Auto-generated method stub
		
	}

	protected void updateDelta(int delta) {
		if (updateDelta < updateInterval)
			updateDelta += delta;
	}

	public synchronized void updateObject(Action update) {
		this.update = update;
		doUpdate();
	}

	private void doUpdate() {
		if (update == null) return;
		
		lifePoints = update.getHitPoints();
		if (lifePoints <= 0){
			die(null);
		}else{
			sendUpdate();
		}
		
		move(new Vector2f(update.getPosX(), update.getPosY()));
		
		setState(ObjectDetails.getState(update.getState()));
		
		switch (ObjectDetails.getState(update.getState())){
			case ATTACK:
				if (update.getTargetPlayerId() < 0 || update.getTargetObjectId() < 0) break;
				
				attackUpdate(update.getTargetPlayerId(), update.getTargetObjectId());
				
				break;
			case MOVE:
				if (update.getMovLocX() < 0 || update.getMovLocY() < 0) break;
				
				moveUpdate(update.getMovLocX(), update.getMovLocY());
				
				break;
			default:
				break;
		}
		
		update = null;
	}
	
	protected void attackUpdate(int tPid, int tOid){
		
	}
	
	protected void moveUpdate(float xLoc, float yLoc){
		
	}
	
	/**
	 * ============================================ UPDATES =================================================
	 */
	protected final void sendUpdate(){
		if (enemy) return;
		
		gameConnection.updateObject(UDPPacketBuilder.getActionPacket(playerId, 
				objectId, objectType, lifePoints, ObjectDetails.getIntState(state), position.x, position.y));
	}
	
	protected final void sendAttackUpdate(int tPid, int tOid){
		if (enemy) return;
		
		gameConnection.updateObject(UDPPacketBuilder.getAttackActionPacket(playerId, 
				objectId, objectType, lifePoints, ObjectDetails.getIntState(State.ATTACK), position.x, position.y, tPid, tOid));
	}
	
	protected final void sendMoveUpdate(float xLoc, float yLoc){
		if (enemy) return;
		
		gameConnection.updateObject(UDPPacketBuilder.getMoveActionPacket(playerId, 
				objectId, objectType, lifePoints, ObjectDetails.getIntState(State.MOVE), position.x, position.y, xLoc, yLoc));
	}
	
	protected final void sendDestroyedUpdate(){
		if (enemy) return;
		
		gameConnection.updateObject(UDPPacketBuilder.getDestroyedActionPacket(playerId, 
				objectId, objectType));
	}
	
	protected final void sendGameOverUpdate(int winnerId){
		if (enemy) return;
		
		gameConnection.updateObject(UDPPacketBuilder.getGameOverPacket(winnerId));
	}
	
	/**
	 * ============================================= STATICS ================================================
	 */
	public enum Orientation{
		UP, DOWN, LEFT, RIGHT
	}
	
	public enum State{
		MOVE, ATTACK, IDLE_75, IDLE_25, DESTROYED
	}
}
