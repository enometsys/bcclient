package com.objects.utils;

import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import com.Main;
import com.objects.GameObject;
import com.objects.attacker.Attacker;
import com.objects.projectile.Projectile;
import com.objects.utils.ObjectDetails.K;
import com.utils.Outliner;

public class AttackerDetails {
	private static final String TAG = AttackerDetails.class.getSimpleName();
	
	/**
	 * =============================== ABOUT THE NewAttacker ===========================================
	 */
	public static boolean isAttacker(int objectType){
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			:
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	:
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			: return true;
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return false;
		}
		
		Main.log.e(TAG, "Error determining if NewAttacker");
		return false;
	}
	
	public static double getDamage(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return 200;
				case K.unit_dragon 			: return 900;
				case K.unit_archer 			: return 120;
				case K.unit_barbarian 		: return 100;
				case K.unit_wizard 			: return 300;
				case K.unit_wallbreaker 	: return 1500;
				case K.unit_giant 			: return 800;
				
				case K.proj_arrow			: return 120;
				case K.proj_cannon_ball		: return 100;
				case K.proj_fire_breath		: return 900;
				case K.proj_magic_attack	: return 300;
				case K.proj_mortar_blast	: return 200;
				
				case K.tow_archer			: return 120;
				case K.tow_cannon			: return 100;
				case K.tow_mortar			: return 200;
				case K.tow_wizard			: return 300;
			}
		}
		
		Main.log.e(TAG, "Error determining damage");
		return -1;
	}
	
	public static boolean isOneTimeAttack(int objectType) {
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			:
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			: return false;
				case K.unit_wallbreaker 	: return true;
				case K.unit_giant 			: return false;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return true;
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			: return false;
			}
		}
		
		Main.log.e(TAG, "Error determining if oneTimeAttack");
		return false;
	}
	
	/**
	 * ================================== SPLASH DAMAGE ===============================================
	 */
	public static boolean hasSplashDamage(int objectType) {
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return false;
				case K.unit_dragon 			: return true;
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			: return false;
				case K.unit_wallbreaker 	:
				case K.unit_giant 			: return true;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		: return false;
				case K.proj_fire_breath		: return true;
				case K.proj_magic_attack	: return false;
				case K.proj_mortar_blast	: return true;
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			: return false;
			}
		}
		
		Main.log.e(TAG, "Error determining if has splash damage");
		return false;
	}
	
	public static Shape getSplashRange(Vector2f pos, int objectType) {
		if (isAttacker(objectType)){
			return Outliner.getOutline(pos, getSplashDamageRadius(objectType));
		}
		
		Main.log.e(TAG, "Error creating splash damage range");
		return null;
	}
	
	public static double getSplashDamageRate(int objectType){
		if (hasSplashDamage(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			: return 0.3;
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	: return 0.3;
				case K.unit_giant 			: return 0.3;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		: return 0.3;
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return 0.3;
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			:
			}
		}
		
		Main.log.e(TAG, "Error determining splash damage rate");
		return -1;
	}
	
	private static int getSplashDamageRadius(int objectType){
		if (hasSplashDamage(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			: return 30;
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	: return 20;
				case K.unit_giant 			: return 20;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		: return 30;
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return 50;
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			:
			}
		}
		
		Main.log.e(TAG, "Error determining splash damage radius");
		return -1;
	}
	
	/**
	 * ==================================== PROJECTILE THROWING ======================================
	 */
	public static boolean throwsProjectile(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return false;
				case K.unit_dragon 			:
				case K.unit_archer 			: return true;
				case K.unit_barbarian 		: return false;
				case K.unit_wizard 			: return true;
				case K.unit_wallbreaker 	:
				case K.unit_giant 			:
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return false;
				
				case K.tow_archer			:
				case K.tow_cannon			:
				case K.tow_mortar			:
				case K.tow_wizard			: return true;
			}
		}
		
		Main.log.e(TAG, "Error determining if can throw projectile");
		return false;
	}
	
	public static Projectile getProjectile(Attacker attacker, GameObject enemy){
		int objectType = getProjectile(attacker.getObjectType());
		if (ProjectileDetails.isProjectile(objectType)){
			return new Projectile(attacker.getPlayerId(), ObjectDetails.getNextGameObjectId(), 
					objectType, attacker.getGameConnection(), attacker.getPosition().copy().add(0.05), enemy);
		}
		
		Main.log.e(TAG, "Error creting projectile");
		return null;
	}
	
	private static int getProjectile(int objectType){
		if (throwsProjectile(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return -1;
				case K.unit_dragon 			: return K.proj_fire_breath;
				case K.unit_archer 			: return K.proj_arrow;
				case K.unit_barbarian 		: 
				case K.unit_wizard 			: return K.proj_magic_attack;
				case K.unit_wallbreaker 	:
				case K.unit_giant 			: return -1;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return -1;
				
				case K.tow_archer			: return K.proj_arrow;
				case K.tow_cannon			: return K.proj_cannon_ball;
				case K.tow_mortar			: return K.proj_arrow;
				case K.tow_wizard			: return K.proj_magic_attack;
			}
		}
		
		Main.log.e(TAG, "Error getting projectile");
		return -1;
	}
	
	/**
	 * ============================== NewAttacker SCANNING CAPABILITIES =================================
	 */
	public static Shape getScanRange(Vector2f pos, int objectType){
		if (isAttacker(objectType)){
			return Outliner.getOutline(pos, getScanRadius(objectType));
		}
		
		Main.log.e(TAG, "Error determining scan range");
		return null;
	}
	
	private static int getScanRadius(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return 200;
				case K.unit_dragon 			: return 200;
				case K.unit_archer 			: return 200;
				case K.unit_barbarian 		: return 200;
				case K.unit_wizard 			: return 200;
				case K.unit_wallbreaker 	: return 200;
				case K.unit_giant 			: return 200;
				
				case K.proj_arrow			: return 300;
				case K.proj_cannon_ball		: return 300;
				case K.proj_fire_breath		: return 300;
				case K.proj_magic_attack	: return 300;
				case K.proj_mortar_blast	: return 300;
				
				case K.tow_archer			: return 270;
				case K.tow_cannon			: return 240;
				case K.tow_mortar			: return 310;
				case K.tow_wizard			: return 290;
			}
		}
		
		Main.log.e(TAG, "Error determining scan radius");
		return -1;
	}
	
	public static int getScanRate(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return 2000;
				case K.unit_dragon 			: return 2000;
				case K.unit_archer 			: return 2000;
				case K.unit_barbarian 		: return 2000;
				case K.unit_wizard 			: return 2000;
				case K.unit_wallbreaker 	: return 2000;
				case K.unit_giant 			: return 2000;
				
				case K.proj_arrow			: return 500;
				case K.proj_cannon_ball		: return 500;
				case K.proj_fire_breath		: return 500;
				case K.proj_magic_attack	: return 500;
				case K.proj_mortar_blast	: return 500;
				
				case K.tow_archer			: return 240;
				case K.tow_cannon			: return 180;
				case K.tow_mortar			: return 220;
				case K.tow_wizard			: return 220;
			}
		}
		
		Main.log.e(TAG, "Error determining scan rate");
		return -1;
	}
	
	
	/**
	 * ============================== NewAttacker ATTACKING CAPABILITIES ================================
	 */
	public static Shape getAttackRange(Vector2f pos, int objectType){
		if (isAttacker(objectType)){
			return Outliner.getOutline(pos, getAttackRadius(objectType));
		}
		
		Main.log.e(TAG, "Error determining attack range");
		return null;
	}
	
	private static int getAttackRadius(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return 20;
				case K.unit_dragon 			: return 100;
				case K.unit_archer 			: return 150;
				case K.unit_barbarian 		: return 18;
				case K.unit_wizard 			: return 120;
				case K.unit_wallbreaker 	: return 20;
				case K.unit_giant 			: return 20;
				
				case K.proj_arrow			: return 10;
				case K.proj_cannon_ball		: return 10;
				case K.proj_fire_breath		: return 10;
				case K.proj_magic_attack	: return 10;
				case K.proj_mortar_blast	: return 10;
				
				case K.tow_archer			: return 280;
				case K.tow_cannon			: return 250;
				case K.tow_mortar			: return 320;
				case K.tow_wizard			: return 300;
			}
		}
		
		Main.log.e(TAG, "Error determining attack radius");
		return -1;
	}

	public static int getAttackRate(int objectType){
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return 500;
				case K.unit_dragon 			: return 500;
				case K.unit_archer 			: return 2000;
				case K.unit_barbarian 		: return 500;
				case K.unit_wizard 			: return 500;
				case K.unit_wallbreaker 	: return 500;
				case K.unit_giant 			: return 500;
				
				case K.proj_arrow			: return 500;
				case K.proj_cannon_ball		: return 500;
				case K.proj_fire_breath		: return 500;
				case K.proj_magic_attack	: return 500;
				case K.proj_mortar_blast	: return 500;
				
				case K.tow_archer			: return 500;
				case K.tow_cannon			: return 500;
				case K.tow_mortar			: return 500;
				case K.tow_wizard			: return 500;
			}
		}
		
		Main.log.e(TAG, "Error determining attack rate");
		return -1;
	}
	
	
	/**
	 * ============================== NewAttacker VALID TARGETS =========================================
	 */	
	public static boolean validTarget(int objectType, GameObject enemy){
		int enemyType = enemy.getObjectType();
		
		if (ProjectileDetails.isProjectile(enemyType)) return false;
		
		if (isAttacker(objectType)){
			switch(objectType){
				case K.unit_hogrider		: return (enemyType != K.unit_dragon);
				case K.unit_dragon 			:
				case K.unit_archer 			: return true;
				case K.unit_barbarian 		: 
				case K.unit_wizard 			: return (enemyType != K.unit_dragon);
				case K.unit_wallbreaker 	: return (enemyType == K.build_wall);
				case K.unit_giant 			:  
				
				case K.proj_arrow			: return true;
				case K.proj_cannon_ball		: return (enemyType != K.unit_dragon);
				case K.proj_fire_breath		: 
				case K.proj_magic_attack	: return true;
				case K.proj_mortar_blast	: return (enemyType != K.unit_dragon);
				
				case K.tow_archer			: return true;
				case K.tow_cannon			: return (enemyType != K.unit_dragon);
				case K.tow_mortar			: return (enemyType != K.unit_dragon);
				case K.tow_wizard			: return true;
			}
		}
		
		Main.log.e(TAG, "Error determining attack rate");
		return false;
	}
}
