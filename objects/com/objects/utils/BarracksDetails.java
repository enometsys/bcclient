package com.objects.utils;

import java.util.HashMap;
import java.util.Map;

import com.Main;
import com.objects.barracks.Barracks;
import com.objects.gamecharacter.GameCharacter;
import com.objects.utils.ObjectDetails.K;

public class BarracksDetails {
	private static final String TAG = BarracksDetails.class.getSimpleName();
		
	private static Map<Integer, Map<Integer, Integer>> gcl;	//game character limit per player
	private static Map<Integer, Map<Integer, Integer>> cgcc;//current game character count per player
	
	static{
		gcl = new HashMap<Integer, Map<Integer, Integer>>();
		cgcc = new HashMap<Integer, Map<Integer, Integer>>();
	}
	
	public static void startPlayer(int playerId){		
		Map<Integer, Integer> temp = new HashMap<Integer, Integer>();
		
		temp.put(K.unit_archer, 		1);
		temp.put(K.unit_barbarian, 		1);
		temp.put(K.unit_wallbreaker, 	2);
		temp.put(K.unit_hogrider, 		1);
		temp.put(K.unit_wizard, 		1);
		temp.put(K.unit_giant, 			1);
		temp.put(K.unit_dragon, 		1);
		
		gcl.put(playerId, temp);
		
		temp = new HashMap<Integer, Integer>();
		
		temp.put(K.unit_archer, 		0);
		temp.put(K.unit_barbarian, 		0);
		temp.put(K.unit_wallbreaker, 	0);
		temp.put(K.unit_hogrider, 		0);
		temp.put(K.unit_wizard, 		0);
		temp.put(K.unit_giant, 			0);
		temp.put(K.unit_dragon, 		0);
		
		cgcc.put(playerId, temp);
	}
	
	public static void resetCounts(){
		gcl.clear();
		cgcc.clear();
	}
	
	/**
	 * ===================================== BARACKS DETAILS ===========================================
	 */	
	public static boolean isBarracks(int objectType){
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			:
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	:
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			: return false;
			
			case K.build_town_hall		: return true;
			case K.build_wall			: return false;
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return true;
		}
		
		Main.log.e(TAG, "Error determining if barracks");
		return false;
	}
	
	public static Map<Integer, Integer> getSpawnTypesAndRates(int objectType){
		if (isBarracks(objectType)){
			Map<Integer, Integer> tempRates = new HashMap<Integer, Integer>();
			
			switch(objectType){
				case K.build_town_hall		: 
					
					tempRates.put(K.unit_archer, 10000);
					tempRates.put(K.unit_barbarian, 5000);
					tempRates.put(K.unit_wallbreaker, 2000);
					
					return tempRates;
					
				case K.build_bar_wizard		:
					
					tempRates.put(K.unit_wizard, 20000);
					
					return tempRates;
					
				case K.build_bar_hog		:
					
					tempRates.put(K.unit_hogrider, 25000);
					
					return tempRates;
					
				case K.build_bar_giant		:
					
					tempRates.put(K.unit_giant, 40000);
					
					return tempRates;
					
				case K.build_bar_dragon		:
					
					tempRates.put(K.unit_dragon, 60000);
					
					return tempRates;
			}
		}
		
		Main.log.e(TAG, "Error determining spawn rates");
		return null;
	}
	
	/**
	 * ===================================== SPAWNING DETAILS ===========================================
	 */
	public static GameCharacter spawnGameCharacter(Barracks br, int objectType){
		if (ObjectDetails.isGameCharacter(objectType)){
			
			objectSpawned(br.getPlayerId(), objectType);
			
			return new GameCharacter(br.getPlayerId(), ObjectDetails.getNextGameObjectId(),
					objectType, br.getGameConnection(), br.getPosition().copy().add(1));
		}
		
		Main.log.e(TAG, "Error spawning non game character from a barracks");
		return null;
	}
	
	public static boolean canSpawn(int playerId, int objectType){		
		if (cgcc.containsKey(playerId)){
			if (cgcc.get(playerId).containsKey(objectType)){
				
				if (gcl.containsKey(playerId)){
					if (gcl.get(playerId).containsKey(objectType)){
						return cgcc.get(playerId).get(objectType) < gcl.get(playerId).get(objectType);
					}
				}
			}
		}
		
		Main.log.e(TAG, "Error on checking if can spawn");
		return false;
	}
	
	public static void objectSpawned(int playerId, int objectType){
		if (cgcc.containsKey(playerId)){
			if (cgcc.get(playerId).containsKey(objectType)){
				cgcc.get(playerId).put(objectType, cgcc.get(playerId).get(objectType) + 1);
				return;
			}
		}
		
		Main.log.e(TAG, "Error on adding gameCharacter count");
	}
	
	public static void gameCharacterDied(int playerId, int objectType){
		if (cgcc.containsKey(playerId)){
			if (cgcc.get(playerId).containsKey(objectType)){
				cgcc.get(playerId).put(objectType, gcl.get(playerId).get(objectType) - 1);
				return;
			}
		}
		
		Main.log.e(TAG, "Error on subtracting gameCharacter count");
	}
	
	public static int getCount(int playerId, int objectType){
		if (cgcc.containsKey(playerId)){
			if (cgcc.get(playerId).containsKey(objectType)){
				return cgcc.get(playerId).get(objectType);
				
			}
		}
		return -1;
	}
	
	public static int getMax(int playerId, int objectType){
		if (gcl.containsKey(playerId)){
			if (gcl.get(playerId).containsKey(objectType)){
				return gcl.get(playerId).get(objectType);
				
			}
		}
		return -1;
	}
}
