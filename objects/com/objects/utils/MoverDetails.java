package com.objects.utils;

import com.Main;
import com.objects.utils.ObjectDetails.K;
import com.states.game.NewGameConnection;

public class MoverDetails {
	private static final String TAG = MoverDetails.class.getSimpleName();
	
	public static final int ASTAR_LIMIT = 500;
	
	/**
	 * =============================== MOVER MOVING CAPABILITIES ===========================================
	 */	
	public static boolean isMover (int objectType){
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			:
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	: return true;
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			:
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return false;
		}
		
		Main.log.e(TAG, "Error determining if mover");
		return false;
	}
	
	public static float getMoveSpeed(int objectType){
		if (isMover(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			:
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	:
				case K.unit_giant 			: return 8f;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return 6f;
			}
		}
		
		Main.log.e(TAG, "Error determining move speed");
		return -1;
	}
	
	public static boolean isPassThroughWalls(int objectType){
		if (isMover(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			: return true;
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	:
				case K.unit_giant 			: return false;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return true;
			}
		}
		
		Main.log.e(TAG, "Error determining if can pass through walls");
		return false;
	}
	
	public static boolean isHoming(int objectType){
		if (isMover(objectType)){
			switch(objectType){
				case K.unit_hogrider		:
				case K.unit_dragon 			: 
				case K.unit_archer 			:
				case K.unit_barbarian 		:
				case K.unit_wizard 			:
				case K.unit_wallbreaker 	:
				case K.unit_giant 			: return true;
				
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return true;
			}
		}
		
		Main.log.e(TAG, "Error determining if homing");
		return false;
	}
	
	public static boolean isValidMoveLocation(int x, int y, NewGameConnection gameConnection){		
		return gameConnection.getObstaclesManager().withinBorder(x, y);
	}
}
