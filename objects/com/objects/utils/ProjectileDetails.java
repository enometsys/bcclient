package com.objects.utils;

import com.Main;
import com.objects.utils.ObjectDetails.K;

public class ProjectileDetails {
	private static final String TAG = ProjectileDetails.class.getSimpleName();
	
	public static boolean isProjectile(int objectType){
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			: return false;
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	: return true;
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			:
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return false;
		}
		
		Main.log.e(TAG, "Error determining if projectile");
		return false;
	}
	
	public static boolean isExpiring(int objectType){
		if (isProjectile(objectType)){
			switch(objectType){
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return true;
			}
		}
		
		Main.log.e(TAG, "Error determining if expiring");
		return false;
	}
	
	public static int getMaxLifetime(int objectType){
		if (isProjectile(objectType)){
			switch(objectType){
				case K.proj_arrow			:
				case K.proj_cannon_ball		:
				case K.proj_fire_breath		:
				case K.proj_magic_attack	:
				case K.proj_mortar_blast	: return 10000;
			}
		}
		
		Main.log.e(TAG, "Error determining if expiring");
		return -1;
	}

}
