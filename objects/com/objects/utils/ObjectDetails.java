package com.objects.utils;

import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import com.Main;
import com.objects.GameObject;
import com.objects.GameObject.Orientation;
import com.objects.GameObject.State;
import com.objects.attacker.Attacker;
import com.objects.barracks.Barracks;
import com.objects.gamecharacter.GameCharacter;
import com.objects.projectile.Projectile;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;
import com.utils.Outliner;

public class ObjectDetails {
	private static final String TAG = ObjectDetails.class.getSimpleName();
	
	private static int		NEXT_NewGame_OBJECT_ID;
	
	public static int	getNextGameObjectId(){
		return NEXT_NewGame_OBJECT_ID++;
	}
	
	/**
	 * =============================== OBJECT DETAILS ===========================================
	 */
	public static boolean isObject(int objectType){
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			:
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	:
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			:
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return true;
		}
		
		Main.log.e(TAG, "Error determining if object. not an object");
		return false;
	}
	
	public static String getObjectName(int objectType){
		switch(objectType){
			case K.unit_hogrider		: return "Hogrider";
			case K.unit_dragon 			: return "Dragon";
			case K.unit_archer 			: return "Archer";
			case K.unit_barbarian 		: return "Barbarian";
			case K.unit_wizard 			: return "Wizard";
			case K.unit_wallbreaker 	: return "Wallbreaker";
			case K.unit_giant 			: return "Giant";
			
			case K.proj_arrow			: return "Arrow";
			case K.proj_cannon_ball		: return "CannonBall";
			case K.proj_fire_breath		: return "FireBreath";
			case K.proj_magic_attack	: return "MagicAttack";
			case K.proj_mortar_blast	: return "MortarBlast";
			
			case K.tow_archer			: return "ArcherTower";
			case K.tow_cannon			: return "Cannon";
			case K.tow_mortar			: return "Mortar";
			case K.tow_wizard			: return "WizardTower";
			
			case K.build_town_hall		: return "TownHall";
			case K.build_wall			: return "Wall";
			case K.build_bar_wizard		: return "Wizard Barracks";
			case K.build_bar_hog		: return "Hogrider Barracks";
			case K.build_bar_giant		: return "Giant Barracks";
			case K.build_bar_dragon		: return "Dragon Barracks";
		}
		
		Main.log.e(TAG, "Error determining object name");
		return "Error!";
	}
	
	public static double getMaxLifePoints(int objectType){
		switch(objectType){
			case K.unit_hogrider		: return 600;
			case K.unit_dragon 			: return 600;
			case K.unit_archer 			: return 600;
			case K.unit_barbarian 		: return 600;
			case K.unit_wizard 			: return 600;
			case K.unit_wallbreaker 	: return 600;
			case K.unit_giant 			: return 600;
			
			case K.proj_arrow			: return 500;
			case K.proj_cannon_ball		: return 500;
			case K.proj_fire_breath		: return 500;
			case K.proj_magic_attack	: return 500;
			case K.proj_mortar_blast	: return 500;
			
			case K.tow_archer			: return 500;
			case K.tow_cannon			: return 500;
			case K.tow_mortar			: return 500;
			case K.tow_wizard			: return 500;
			
			case K.build_town_hall		: return 500;
			case K.build_wall			: return 500;
			case K.build_bar_wizard		: return 500;
			case K.build_bar_hog		: return 500;
			case K.build_bar_giant		: return 500;
			case K.build_bar_dragon		: return 500;
		}
		
		Main.log.e(TAG, "Error determining max life points");
		return -1;
	}
	
	public static Shape getOutline(Vector2f pos, int objectType){
		return Outliner.getOutline(pos, getOutlineRadius(objectType));
	}
	
	private static int getOutlineRadius(int objectType){
		switch(objectType){
			case K.unit_hogrider		: return 12;
			case K.unit_dragon 			: return 12;
			case K.unit_archer 			: return 12;
			case K.unit_barbarian 		: return 12;
			case K.unit_wizard 			: return 12;
			case K.unit_wallbreaker 	: return 12;
			case K.unit_giant 			: return 12;
			
			case K.proj_arrow			: return 12;
			case K.proj_cannon_ball		: return 12;
			case K.proj_fire_breath		: return 12;
			case K.proj_magic_attack	: return 12;
			case K.proj_mortar_blast	: return 12;
			
			case K.tow_archer			: return 12;
			case K.tow_cannon			: return 12;
			case K.tow_mortar			: return 12;
			case K.tow_wizard			: return 12;
			
			case K.build_town_hall		: return 12;
			case K.build_wall			: return 12;
			case K.build_bar_wizard		: return 12;
			case K.build_bar_hog		: return 12;
			case K.build_bar_giant		: return 12;
			case K.build_bar_dragon		: return 12;
		}
		
		Main.log.e(TAG, "Error determining outline radius");
		return -1;
	}
	
	public static String getObjectTypeName(int objectType){
		return null;
	}
	
	public static int	getIntState(State state){
		int intState = -1;
		switch(state){
			case ATTACK:
				intState = 1; break;
			case DESTROYED:
				intState = 2; break;
			case IDLE_25:
				intState = 3; break;
			case IDLE_75:
				intState = 4; break;
			case MOVE:
				intState = 5; break;
			default:
				break;
		}
		
		return intState;
	}
	
	public static int	getIntOrientation(Orientation orientation){
		int intOrientation = -1;
		switch(orientation){
			case DOWN:
				intOrientation = 1; break;
			case LEFT:
				intOrientation = 2; break;
			case RIGHT:
				intOrientation = 3; break;
			case UP:
				intOrientation = 4; break;
			default:
				break;
				
		}
		
		return intOrientation;
	}
	
	public static State getState(int state){
		State tempState = null;
		
		switch(state){
			case 1: tempState = State.ATTACK; break;
			case 2: tempState = State.DESTROYED; break;
			case 3: tempState = State.IDLE_25; break;
			case 4: tempState = State.IDLE_75; break;
			case 5: tempState = State.MOVE; break;
		}
		
		return tempState;
	}
	
	public static Orientation getOrientation(int orientation){
		Orientation Orienation = null;
		
		switch(orientation){
			case 1: Orienation = Orientation.DOWN; break;
			case 2: Orienation = Orientation.LEFT; break;
			case 3: Orienation = Orientation.RIGHT; break;
			case 4: Orienation = Orientation.UP; break;
		}
		
		return Orienation;
	}

	public static boolean isSameObject(int objectType, int comparedToobjectType){
		if (objectType < 0 || objectType>21){
			Main.log.e(TAG, "Invalid objectType");
			return false;
		}
		return objectType == comparedToobjectType;
	}

	public static final class K{
		public static final int unit_hogrider 		= 0;
		public static final int unit_dragon 		= 1;
		public static final int unit_archer 		= 2;
		public static final int unit_barbarian 		= 3;
		public static final int unit_wizard 		= 4;
		public static final int unit_wallbreaker 	= 5;
		public static final int unit_giant 			= 6;
		
		public static final int proj_arrow			= 7;
		public static final int proj_cannon_ball	= 8;
		public static final int proj_fire_breath	= 9;
		public static final int proj_magic_attack	= 10;
		public static final int proj_mortar_blast	= 11;
		
		public static final int tow_archer			= 12;
		public static final int tow_cannon			= 13;
		public static final int tow_mortar			= 14;
		public static final int tow_wizard			= 15;
		
		public static final int build_town_hall		= 16;
		public static final int build_wall			= 17;
		public static final int build_bar_wizard	= 18;
		public static final int build_bar_hog		= 19;
		public static final int build_bar_giant		= 20;
		public static final int build_bar_dragon	= 21;
	}

	public static GameObject spawnUpdateObject(NewGameConnection gameConnection, int playerId, int objectId, int objectType, Action update) {
		
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			: return new GameCharacter(playerId, objectId, objectType, gameConnection, update);
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	: return new Projectile(playerId, objectId, objectType, gameConnection, update);
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			: return new Attacker(playerId, objectId, objectType, gameConnection, update); 
			
			case K.build_town_hall		:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return new Barracks(playerId, objectId, objectType, gameConnection, update); 
			
			case K.build_wall			: return new GameObject(playerId, objectId, objectType, gameConnection, update);
		}
		
		Main.log.e(TAG, "Invalid NewGameObject Id");
		return null;
	}

	public static boolean isMapObstacle(int objectType) {
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			:
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	: return false;
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			:
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return true;
		}
		
		return false;
	}

	public static boolean isGameCharacter(int objectType) {
		switch(objectType){
			case K.unit_hogrider		:
			case K.unit_dragon 			:
			case K.unit_archer 			:
			case K.unit_barbarian 		:
			case K.unit_wizard 			:
			case K.unit_wallbreaker 	:
			case K.unit_giant 			: return true;
			
			case K.proj_arrow			:
			case K.proj_cannon_ball		:
			case K.proj_fire_breath		:
			case K.proj_magic_attack	:
			case K.proj_mortar_blast	:
			
			case K.tow_archer			:
			case K.tow_cannon			:
			case K.tow_mortar			:
			case K.tow_wizard			:
			
			case K.build_town_hall		:
			case K.build_wall			:
			case K.build_bar_wizard		:
			case K.build_bar_hog		:
			case K.build_bar_giant		:
			case K.build_bar_dragon		: return false;
		}
		
		Main.log.e(TAG, "Error determining max life points");
		return false;
	}
}
