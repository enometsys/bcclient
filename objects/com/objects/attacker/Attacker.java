package com.objects.attacker;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.Main;
import com.objects.GameObject;
import com.objects.projectile.Projectile;
import com.objects.utils.AttackerDetails;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;
import com.utils.Outliner;

public class Attacker extends GameObject{
	private static final String TAG = Attacker.class.getSimpleName();
	
	private GameObject  	target;
	private double			damage;
	private final boolean 	throwsProjectile;
	private final boolean 	oneTimeAttack;
	
	private Shape			attackRange;
	private int				attackRate;
	private	int				attackDelta;
	
	private Shape 			scanRange;
	private int				scanRate;
	private	int				scanDelta;
	
	private final boolean	hasSplashDamage;
	private Shape 			splashRange;
	private double 			splashDamageRate;
	
	
	
	protected Attacker(int playerId, int objectId, int objectType, NewGameConnection gameConnection,
			Vector2f position, Action update, GameObject target) {
		super(playerId, objectId, objectType, gameConnection, position, update);
		
		this.target = target;
		
		this.damage = AttackerDetails.getDamage(objectType);
		this.throwsProjectile = AttackerDetails.throwsProjectile(objectType);
		this.oneTimeAttack = AttackerDetails.isOneTimeAttack(objectType);
		this.attackRange = AttackerDetails.getAttackRange(getPosition(), objectType);
		this.attackRate = AttackerDetails.getAttackRate(objectType);
		this.attackDelta = this.attackRate;
		
		this.scanRange = AttackerDetails.getScanRange(getPosition(), objectType);
		this.scanRate = AttackerDetails.getScanRate(objectType);
		this.scanDelta = this.scanRate;
		
		this.hasSplashDamage = AttackerDetails.hasSplashDamage(objectType);
		
		if (this.hasSplashDamage){
			this.splashRange = AttackerDetails.getSplashRange(getPosition(), objectType);
			this.splashDamageRate = AttackerDetails.getSplashDamageRate(objectType);
		}
	}
	
	//For projectile, i.e., with initial target
	protected Attacker(int playerId, int objectId, int objectType, NewGameConnection gameConnection,
			Vector2f position, GameObject target) {
		this(playerId, objectId, objectType, gameConnection, position, null, target);
	}
	
	//for update
	public Attacker(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		this(playerId, objectId, objectType, gameConnection, null, update, null);
	}
	
	//for new spawn
	public Attacker(int playerId, int objectId, int objectType, NewGameConnection gameConnection,
			Vector2f position) {
		this(playerId, objectId, objectType, gameConnection, position, null, null);
	}

	
	/**
	 * ==================================== WORLD FUNCTIONS ==========================================
	 */
	public void rightClickAction(int x, int y, GameObject target){
		setTarget(target);
	}
	
	
	/**
	 * ======================================= OVERRIDES =============================================
	 */
	protected void attackUpdate(int tPid, int tOid){
		setTarget(gameConnection.getTargetObject(tPid, tOid));
	}

	@Override
	protected final void move(Vector2f newPosition) {
		super.move(newPosition);
		
		attackRange.setCenterX(newPosition.x);
		attackRange.setCenterY(newPosition.y);
		scanRange.setCenterX(newPosition.x);
		scanRange.setCenterY(newPosition.y);
	}

	@Override
	protected void updateDelta(int delta) {
		super.updateDelta(delta);
		
		if (attackDelta < attackRate)
			attackDelta += delta;
		
		if (scanDelta < scanRate)
			scanDelta += delta;
	}
	
	@Override
	protected void updateGameUnp(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		if (withinAttackRange())
			attackTarget();
		else 
			scanForEnemy();
	}

	@Override
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY, int xLoc,
			int yLoc) {
		yLoc = super.renderDebug(gc, sbg, g, mapX, mapY, xLoc, yLoc);
		
		g.setColor(Color.yellow);
		g.draw(Outliner.getVisibleOutline(scanRange, mapX, mapY));
		
		g.setColor(Color.orange);
		g.draw(Outliner.getVisibleOutline(attackRange, mapX, mapY));
		
		return yLoc;
	}
	
	

	/**
	 * ====================================== ATTACKER FUNCTIONS ======================================
	 */
	protected final GameObject getTarget(){
		if (target == null){
			setIdle();
			return null;
		}
		
		if (!target.isActive()){
			setIdle();
			target = null;
		}
		
		return target;
	}
	
	protected final boolean withinAttackRange(){
		GameObject targetObject = getTarget();
		
		if (targetObject == null) return false;
		if (!targetObject.isEnemy()) return false;
		
		return attackRange.contains(targetObject.getPosition().x, targetObject.getPosition().y);
		//return attackRange.intersects(targetObject.getOutline());
	}
	
	protected final boolean withinScanRange(){
		GameObject targetObject = getTarget();
		
		if (targetObject == null) return false;
		if (!targetObject.isEnemy()) return false;
		
		return scanRange.contains(targetObject.getPosition().x, targetObject.getPosition().y);
		//return scanRange.intersects(targetObject.getOutline());
	}
	
	protected final void attackTarget() {
		if (attackDelta >= attackRate){
			
			setState(State.ATTACK);
			
			if (throwsProjectile){
				Projectile tempProjectile = AttackerDetails.getProjectile(this, target);
				if (tempProjectile != null){
					gameConnection.addToSpawnQueue(tempProjectile);
				}else{
					Main.log.e(TAG, "Error spawning projectile!");
				}
			}else{
				
				target.attack(damage, this);
				
				if (hasSplashDamage){
					for (GameObject go: gameConnection.getEnemyUnits(getPlayerId(),false, target)){
						if (splashRange.intersects(go.getOutline())){
							go.attack(damage * splashDamageRate, this);
						}
					}
				}
				
				if (oneTimeAttack){
					target = null;
					deactivate();
				}
				
			}
			
			attackDelta = 0;
		}
	}
	
	protected final void scanForEnemy() {
		if (isControlled()) return;
			
		if (scanDelta >= scanRate){
			
			for(GameObject go: gameConnection.getEnemyUnits(getPlayerId(),false, null)){
				if (Projectile.class.isInstance(go)) continue;	//Do not attack projectiles
				if (scanRange.intersects(go.getOutline()) && AttackerDetails.validTarget(objectType, go)){
					setTarget(go);
					return;
				}
			}
			scanDelta = 0;
		}		
	}
	
	private void setTarget(GameObject go){
		if (go == null) return;
		target = go;
		if (!go.isEnemy()){
			sendAttackUpdate(go.getPlayerId(), go.getObjectId());
		}
	}
}
