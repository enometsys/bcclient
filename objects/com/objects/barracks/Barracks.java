package com.objects.barracks;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.objects.GameObject;
import com.objects.utils.BarracksDetails;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;

public class Barracks extends GameObject{
	private Map<Integer, Integer>	spawnRates;
	private Map<Integer, Integer>	spawnDeltas;
	
	//with updates
	public Barracks(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		super(playerId, objectId, objectType, gameConnection, null, update);
		this.spawnRates 	= BarracksDetails.getSpawnTypesAndRates(objectType);
		this.spawnDeltas 	= new HashMap<Integer, Integer>();
		
		for(int gId:spawnRates.keySet()){
			spawnDeltas.put(gId, 0);
		}
	}
	
	//from spawn
	public Barracks(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position) {
		super(playerId, objectId, objectType, gameConnection, position, null);
		
		this.spawnRates 	= BarracksDetails.getSpawnTypesAndRates(objectType);
		this.spawnDeltas 	= new HashMap<Integer, Integer>();
		
		for(int gId:spawnRates.keySet()){
			spawnDeltas.put(gId, 0);
		}
	}
	
	/**
	 * ======================================= OVERRIDES =============================================
	 */
	@Override
	protected void updateDelta(int delta) {
		for(int gId:spawnDeltas.keySet()){
			if (spawnDeltas.get(gId) < spawnRates.get(gId))
				spawnDeltas.put(gId, spawnDeltas.get(gId) + delta);
		}
	}
	
	@Override
	protected void updateGameUnp(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		for(int gId:spawnDeltas.keySet()){
			if (spawnDeltas.get(gId) >= spawnRates.get(gId)){
				
				if (BarracksDetails.canSpawn(getPlayerId(), gId)){
					gameConnection.addToSpawnQueue(BarracksDetails.spawnGameCharacter(this, gId));
					spawnDeltas.put(gId, 0);
				}
				
			}
		}
	}
	
	
}
