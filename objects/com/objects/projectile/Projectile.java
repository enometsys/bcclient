package com.objects.projectile;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;

import com.objects.GameObject;
import com.objects.mover.Mover;
import com.objects.utils.ProjectileDetails;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;

public class Projectile extends Mover{
	private final boolean 	expiring;
	private int 			maxLifeTime;
	private int 			lifeTimeDelta;
	
	private Projectile(int playerId, int objectId, int objectType, NewGameConnection gameConnection,
			Vector2f position, Action update, GameObject target) {
		super(playerId, objectId, objectType, gameConnection, position, update, target);
		
		this.expiring 		= ProjectileDetails.isExpiring(objectType);
				
		if (this.expiring){
			this.maxLifeTime 	= ProjectileDetails.getMaxLifetime(objectType);
			this.lifeTimeDelta 	= 0;
		}		
	}
	
	public Projectile(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		this(playerId, objectId, objectType, gameConnection, null, update, null);
	}
	
	public Projectile(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position, GameObject target) {
		this(playerId, objectId, objectType, gameConnection, position, null, target);
	}
	
	/**
	 * ======================================= OVERRIDES =============================================
	 */
	@Override
	protected void updateDelta(int delta) {
		super.updateDelta(delta);
		if (lifeTimeDelta < maxLifeTime)
			lifeTimeDelta += delta;
	}
	
	@Override
	protected void updateGameUnp(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {		
		
		if (!isControlled())
			if ((expiring && (lifeTimeDelta >= maxLifeTime)) ||
					getTarget() == null){
				deactivate();
				return;
			}
		
		super.updateGameUnp(gc, sbg, delta, mapX, mapY);
	}
	
}
