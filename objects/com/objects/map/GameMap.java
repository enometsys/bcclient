package com.objects.map;
import java.util.HashMap;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.tiled.TiledMap;
import org.newdawn.slick.util.pathfinding.PathFindingContext;
import org.newdawn.slick.util.pathfinding.TileBasedMap;

import com.JoinedMatch;
import com.Main;
import com.Player.MatchPlayer;
import com.objects.GameObject;
import com.objects.attacker.Attacker;
import com.objects.barracks.Barracks;
import com.objects.mover.Mover;
import com.objects.utils.ObjectDetails;
import com.states.game.GameMapState.ObstaclesManager;
import com.utils.Outliner;
import com.utils.Util;
import com.states.game.NewGameConnection;


public final class GameMap implements TileBasedMap, ObstaclesManager{
	private static final String TAG = GameMap.class.getSimpleName();
	
	private TiledMap tiledMap;
	
	private Shape[][] borderMat;		//border
	private Shape[][] terrainMat;		//terrain
	private Shape[][] obstaclesMat;		//obstacles
	
	public GameMap(int mapId, int scrollSpeed){
		if (mapId >= 0 && K.PATH_MAP.length > mapId){
			try {
				tiledMap = new TiledMap(K.PATH_MAP[mapId]);
				
				Main.log.d(TAG, "TiledMap successfully created!");
				
				//Set border
				borderMat = new Shape[tiledMap.getHeight()][tiledMap.getWidth()];
				int border = tiledMap.getLayerIndex("border");
				for(int i = 0; i < tiledMap.getHeight(); i++){
					for(int j=0; j < tiledMap.getWidth(); j++){
						if (tiledMap.getTileId(i, j, border) != 0){
							borderMat[i][j] = new Circle(i*32 + 28, j*32 + 32, 10);
						}
					}
				}
				
				//Set border
				terrainMat = new Shape[tiledMap.getHeight()][tiledMap.getWidth()];
				int terrain = tiledMap.getLayerIndex("terrain");
				for(int i = 0; i < tiledMap.getHeight(); i++){
					for(int j=0; j < tiledMap.getWidth(); j++){
						if (tiledMap.getTileId(i, j, terrain) != 0){
							terrainMat[i][j] = new Circle(i*32 + 28, j*32 + 32, 10);
						}
					}
				}
				
				obstaclesMat = new Shape[tiledMap.getHeight()][tiledMap.getWidth()];
				
			} catch (SlickException e) {
				Main.log.e(TAG, "Error creating map: " + e.getMessage()); 
				
				e.printStackTrace();
			}
		}else{
			Main.log.e(TAG, "Error creating map. Map Doesn't exist");
		}
	}
	
	public void initPlayerBase(JoinedMatch currMatch,NewGameConnection gameConnection, int playerId){		
		MatchPlayer tempPlayer = currMatch.getPlayer(playerId);

		String pref = "pl-" + tempPlayer.getMapLocation() + "-";
		
		Main.log.d(TAG, "Initializing Map Location " + tempPlayer.getMapLocation());
		
		java.util.Map<String, Integer> iDs = new HashMap<String,Integer>();
		
		iDs.put(K.TOWN_HALL, 			tiledMap.getLayerIndex(pref + K.TOWN_HALL));
		iDs.put(K.WALLS, 				tiledMap.getLayerIndex(pref + K.WALLS));
		iDs.put(K.WIZARD_BARRACKS, 		tiledMap.getLayerIndex(pref + K.WIZARD_BARRACKS));
		iDs.put(K.HOGRIDER_BARRACKS, 	tiledMap.getLayerIndex(pref + K.HOGRIDER_BARRACKS));
		iDs.put(K.GIANT_BARRACKS, 		tiledMap.getLayerIndex(pref + K.GIANT_BARRACKS));
		iDs.put(K.DRAGON_BARRACKS, 		tiledMap.getLayerIndex(pref + K.DRAGON_BARRACKS));
		iDs.put(K.ARCHER_TOWER, 		tiledMap.getLayerIndex(pref + K.ARCHER_TOWER));
		iDs.put(K.WIZARD_TOWER, 		tiledMap.getLayerIndex(pref + K.WIZARD_TOWER));
		iDs.put(K.CANNON, 				tiledMap.getLayerIndex(pref + K.CANNON));
		iDs.put(K.MORTAR, 				tiledMap.getLayerIndex(pref + K.MORTAR));
		
		for(String key:iDs.keySet()){
			for(int i = 0; i < tiledMap.getHeight(); i++){
				for(int j=0; j < tiledMap.getWidth(); j++){
					if (tiledMap.getTileId(i, j, iDs.get(key)) != 0){
						
						GameObject temp = null;
						
						switch(key){
						case K.TOWN_HALL:
							temp = new Barracks(playerId, ObjectDetails.getNextGameObjectId(),
										ObjectDetails.K.build_town_hall, gameConnection, new Vector2f((i*32), (j*32)));
							break;
						case K.WALLS:
							temp = new GameObject(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.build_wall, gameConnection, new Vector2f((i*32)+16,(j*32)+16));
							break;
						case K.WIZARD_BARRACKS:
							temp = new Barracks(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.build_bar_wizard, gameConnection, new Vector2f((i*32)+16, (j*32)+16));							
							break;
						case K.HOGRIDER_BARRACKS:
							temp = new Barracks(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.build_bar_hog, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.GIANT_BARRACKS:
							temp = new Barracks(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.build_bar_giant, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.DRAGON_BARRACKS:
							temp = new Barracks(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.build_bar_dragon, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.ARCHER_TOWER:
							temp = new Attacker(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.tow_archer, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.WIZARD_TOWER:
							temp = new Attacker(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.tow_wizard, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.CANNON:
							temp = new Attacker(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.tow_cannon, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						case K.MORTAR:
							temp = new Attacker(playerId, ObjectDetails.getNextGameObjectId(),
									ObjectDetails.K.tow_mortar, gameConnection, new Vector2f((i*32)+16, (j*32)+16));
							break;
						}
						
						if (temp != null) gameConnection.addToSpawnQueue(temp);
					}
				}
			}
		}
	}
	
	
	
	/**
	 * ========================================  OBSTACLES MGT  ===========================================
	 */
	@Override
	public void addToObstacles(GameObject gameObject){
		
		if (Mover.class.isInstance(gameObject)) return;
		
		int x = (int) (gameObject.getPosition().getX() / 32);
		int y = (int) (gameObject.getPosition().getY() / 32);
		
		obstaclesMat[x][y] = gameObject.getOutline();
	}
	
	@Override
	public void removeFromObstacles(GameObject gameObject){
		
		if (Mover.class.isInstance(gameObject)) return;
		
		int x = (int) (gameObject.getPosition().getX() / 32);
		int y = (int) (gameObject.getPosition().getY() / 32);
		
		obstaclesMat[x][y] = null;
	}
	
	@Override
	public boolean withinBorder(int x, int y) {
		if (x < 0 || y < 0) return false;
		return ( x < tiledMap.getWidth() * tiledMap.getTileWidth()) 
				&& ( y < tiledMap.getHeight() * tiledMap.getTileHeight());
	}
	
	@Override
	public boolean isBlocked(Line ln) {
		return true;
	}
	
	/**
	 * ========================================  MAP RENDERING  ===========================================
	 */
	public final void renderGame(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY, boolean renderGeo) {
		tiledMap.render(mapX, mapY);
		if (renderGeo) printGeo(g, mapX, mapY);
	}
	
	private void printGeo(Graphics g, int mapX, int mapY) {
		for(int i = 0; i < tiledMap.getHeight(); i++){
			for(int j=0; j < tiledMap.getWidth(); j++){
				if (borderMat[i][j] != null){
					Shape temp = borderMat[i][j];
					g.setColor(Color.white);
					g.draw(Outliner.getVisibleOutline(temp, mapX, mapY));
					g.setColor(Color.pink);
					//g.draw(borderMat[i][j]);
				}
				
				if (terrainMat[i][j] != null){
					Shape temp = terrainMat[i][j];
					g.setColor(Color.white);
					g.draw(Outliner.getVisibleOutline(temp, mapX, mapY));
					g.setColor(Color.pink);
					//g.draw(terrainMat[i][j]);
				}
				
				if (obstaclesMat[i][j] != null){
					Shape temp = obstaclesMat[i][j];
					g.setColor(Color.white);
					g.draw(Outliner.getVisibleOutline(temp, mapX, mapY));
					g.setColor(Color.pink);
					//g.draw(obstaclesMat[i][j]);
				}
			}
		}
		
	}
	
	
	/**
	 * =====================================  SCROLLING  ========================================
	 */
	public int getMarLowX(int scrollSpeed){
		return scrollSpeed + 15;
	}
	
	public int getMarLowY(int scrollSpeed){
		return scrollSpeed + 15;
	}
	
	public int getMarUpX(int scrollSpeed){
		return ((tiledMap.getWidth() * tiledMap.getTileWidth()) - Util.X_SIZE + (scrollSpeed + 15)) * -1;
	}
	
	public int getMarUpY(int scrollSpeed){
		return ((tiledMap.getHeight() * tiledMap.getTileHeight()) - Util.Y_SIZE + (scrollSpeed + 15)) * -1;
	}

	
	
	
	/**
	 * ====================================  PATH FINDING  ========================================
	 */
	@Override
	public boolean blocked(PathFindingContext mov, int i, int j) {
		if (borderMat[i][j] != null) return true;
		if (terrainMat[i][j] != null) return true;
		if (((Mover)mov.getMover()).isPassThroughWalls()) return false;
		if (obstaclesMat[i][j] != null) return true;
		return false;
	}

	@Override
	public float getCost(PathFindingContext arg0, int i, int j) {
		return 1;
	}

	@Override
	public int getHeightInTiles() {
		return tiledMap.getHeight();
	}

	@Override
	public int getWidthInTiles() {
		return tiledMap.getWidth();
	}

	@Override
	public void pathFinderVisited(int arg0, int arg1) {
		
	}
	
	
	/**
	 * ========================================  CONSTANTS  =======================================
	 */
	public static final class K{
		public static final String[] PATH_MAP = new String[1];
		static{
			PATH_MAP[0] = "res/maps/map-default.tmx";
		}
		
		public static final String TOWN_HALL = "townHall";
		public static final String WALLS = "walls";
		public static final String WIZARD_BARRACKS = "wizard-barracks";
		public static final String HOGRIDER_BARRACKS = "hogRider-barracks";
		public static final String GIANT_BARRACKS = "giant-barracks";
		public static final String DRAGON_BARRACKS = "dragon-barracks";
		public static final String ARCHER_TOWER = "archer-tower";
		public static final String WIZARD_TOWER = "wizard-tower";
		public static final String CANNON = "cannon";
		public static final String MORTAR = "mortar";
		
	}

	public int getMapXCoord(int mapLocation) {
		switch (mapLocation){
			case 0: return 0;
			case 1: return 1120 * -1;
			case 2: return 0;
			case 3: return 1120 * -1;
			case 4: return 480 * -1;
		}
		
		return 0;
	}

	public int getMapYCoord(int mapLocation) {
		switch (mapLocation){
			case 0: return 0;
			case 1: return 1660 * -1;
			case 2: return 1660 * -1;
			case 3: return 0;
			case 4: return 800 * -1;
		}
		
		return 0;
	}
	
}
