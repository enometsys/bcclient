package com.objects.mover;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.pathfinding.AStarPathFinder;
import org.newdawn.slick.util.pathfinding.Path;
import org.newdawn.slick.util.pathfinding.PathFinder;

import com.objects.GameObject;
import com.objects.attacker.Attacker;
import com.objects.utils.MoverDetails;
import com.packet.udp.Action;
import com.states.game.NewGameConnection;
import com.utils.Outliner;

public class Mover extends Attacker implements org.newdawn.slick.util.pathfinding.Mover {

	private float moveSpeed;
	private boolean passThroughWalls;

	private Vector2f velocity; // velocity of movement;
	private Vector2f moveLocation; // move destination

	protected Mover(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position,
			Action update, GameObject target) {
		super(playerId, objectId, objectType, gameConnection, position, update, target);
		this.moveSpeed = MoverDetails.getMoveSpeed(objectType);
		this.passThroughWalls = MoverDetails.isPassThroughWalls(objectType);
		this.velocity = null;
		this.moveLocation = null;

		finder = new AStarPathFinder(gameConnection.getMap(), MoverDetails.ASTAR_LIMIT, true);

		if (!enemy)
			moveToLocation(getPosition());
	}

	// with update
	public Mover(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Action update) {
		this(playerId, objectId, objectType, gameConnection, null, update, null);
	}

	// with target, for projectile
	protected Mover(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position,
			GameObject target) {
		this(playerId, objectId, objectType, gameConnection, position, null, target);
	}

	// for new spawn
	public Mover(int playerId, int objectId, int objectType, NewGameConnection gameConnection, Vector2f position) {
		this(playerId, objectId, objectType, gameConnection, position, null, null);
	}

	/**
	 * ==================================== WORLD FUNCTIONS ==========================================
	 */
	public final void rightClickAction(int x, int y, GameObject target) {
		super.rightClickAction(x, y, target);
		if (getTarget() != null)
			findPath(getTarget().getPosition());
		else{
			moveLocation = null;
			findPath(x, y);
		}
	}

	/**
	 * ======================================= OVERRIDES =============================================
	 */
	protected void moveUpdate(float xLoc, float yLoc){
		moveLocation = new Vector2f(xLoc, yLoc);
	}

	@Override
	protected void updateGameUnp(GameContainer gc, StateBasedGame sbg, int delta, int mapX, int mapY) {
		if (withinAttackRange()){
			
			attackTarget();

		}else if (!onMovLocation(delta)) {

			setState(State.MOVE);
			velocity = moveLocation.copy().sub(getPosition()).normalise();
			move(getPosition().add(velocity.copy().scale(delta / moveSpeed)));

		}else{
			
			if (!nextStep()){
				setIdle();
				scanForEnemy();
			}
		}
	}

	@Override
	protected int renderDebug(GameContainer gc, StateBasedGame sbg, Graphics g, int mapX, int mapY, int xLoc,
			int yLoc) {
		yLoc = super.renderDebug(gc, sbg, g, mapX, mapY, xLoc, yLoc);

		if (moveLocation != null){
			g.setColor(Color.magenta);
			g.draw(Outliner.getVisibleOutline(Outliner.getOutline(moveLocation, 5), mapX, mapY));
		}

		return yLoc;
	}

	/**
	 * ==================================== MOVER FUNCTIONS ==========================================
	 */
	public final boolean isPassThroughWalls() {
		return passThroughWalls;
	}

	private void moveToLocation(Vector2f destination) {
		GameObject target = getTarget();

		if (target != null)
			this.moveLocation = destination;
		else
			this.moveLocation = afterCollisionDetection(destination);
	}

	private boolean onMovLocation(int delta) {
		GameObject target = getTarget();

		if (target != null) {
			if (target.isEnemy()) {
				if (withinAttackRange()) return true;
				moveLocation = target.getPosition();
			} else {
				if (getOutline().intersects(target.getOutline())) return true;
				findPath(target.getPosition()); return false;
			}
		}

		if (moveLocation == null)
			return true;
		return getPosition().distance(moveLocation) <= delta / moveSpeed;
	}

	/**
	 * ==================================== PATH FINDING ==========================================
	 */
	private PathFinder finder;
	private Path path;
	private int currStep;

	private void findPath(Vector2f position) {
		findPath((int) position.x, (int) position.y);
	}

	private void findPath(int x, int y) {
		path = null;
		currStep = 0;

		if (!gameConnection.getObstaclesManager().withinBorder(x, y))
			return;

		int startX = (int) (getPosition().getX() / 32);
		int startY = (int) (getPosition().getY() / 32);

		if (startX < 0 || startY < 0) return;
		
		int destX = x / 32;
		int destY = y / 32;

		offsetX = x % 32;
		offsetY = y % 32;

		path = finder.findPath(this, startX, startY, destX, destY);

		if (path != null) {
			nextStep();
		}
	}

	private float offsetX;
	private float offsetY;

	private boolean nextStep() {
		if (path == null) return false;

		if (currStep < path.getLength()) {

			int i = maxWithoutCollision(currStep);

			float x = (path.getX(i) * 32) + offsetX;
			float y = (path.getY(i) * 32) + offsetY;

			sendMoveUpdate(x, y);

			moveToLocation(new Vector2f(x, y));

			currStep = i + 1;
			return true;
			
		}

		path = null;		
		return false;
	}

	private int maxWithoutCollision(int currStep) {
		// TODO Draw a line to the farthest in the path where there is not
		// collision

		for (int i = (path.getLength() - 1); i > currStep; i--) {
			Line ln = new Line(getPosition(), new Vector2f(path.getX(i) * 32, path.getY(i) * 32));

			if (!gameConnection.getObstaclesManager().isBlocked(ln))
				return i;
		}

		return currStep;
	}

	/**
	 * ==================================== SQUAD FORMATION ==========================================
	 */
	private int movedCollidingFrom;

	private Vector2f afterCollisionDetection(Vector2f moveLocation) {
		Vector2f desiredMoveLocation = moveLocation.copy();

		Boolean[] collidesOnDirection = new Boolean[9];
		for (int i = 0; i < 9; i++)
			collidesOnDirection[i] = false;

		for (GameObject qwe : gameConnection.getAllyUnits(playerId, true, this)) {
			if (!Mover.class.isInstance(qwe))
				continue;

			Mover go = (Mover) qwe;

			int collidesOn = go.collidesOnMoveLocation(collidesOnDirection, desiredMoveLocation);
			switch (collidesOn) {
			case CollidesOn.CENTER:
			case CollidesOn.TOP:
			case CollidesOn.BOTTOM:
			case CollidesOn.LEFT:
			case CollidesOn.RIGHT:
			case CollidesOn.TOP_LEFT:
			case CollidesOn.TOP_RIGHT:
			case CollidesOn.BOT_LEFT:
			case CollidesOn.BOT_RIGHT:
				collidesOnDirection[collidesOn] = true;
				break;
			default:
				break;
			}

			if (collidesOnDirection[CollidesOn.CENTER] && collidesOnDirection[CollidesOn.TOP]
					&& collidesOnDirection[CollidesOn.BOTTOM] && collidesOnDirection[CollidesOn.LEFT]
					&& collidesOnDirection[CollidesOn.RIGHT] && collidesOnDirection[CollidesOn.TOP_LEFT]
					&& collidesOnDirection[CollidesOn.TOP_RIGHT] && collidesOnDirection[CollidesOn.BOT_LEFT]
					&& collidesOnDirection[CollidesOn.BOT_RIGHT]) {
				break;
			}
		}

		for (int i = 0; i < 9; i++) {
			if (!collidesOnDirection[i]) {
				movedCollidingFrom = -1;
				return adjustedVector(desiredMoveLocation, i);
			}
		}

		Random r = new Random();
		// randomize a direction but the center
		int randomDirection = r.nextInt(8) + 1;
		while (randomDirection == movedCollidingFrom) {
			randomDirection = r.nextInt(8) + 1;
		}

		switch (randomDirection) {
		case CollidesOn.TOP:
			movedCollidingFrom = CollidesOn.BOTTOM;
			break;
		case CollidesOn.BOTTOM:
			movedCollidingFrom = CollidesOn.TOP;
			break;
		case CollidesOn.LEFT:
			movedCollidingFrom = CollidesOn.RIGHT;
			break;
		case CollidesOn.RIGHT:
			movedCollidingFrom = CollidesOn.LEFT;
			break;
		case CollidesOn.TOP_LEFT:
			movedCollidingFrom = CollidesOn.BOT_RIGHT;
			break;
		case CollidesOn.TOP_RIGHT:
			movedCollidingFrom = CollidesOn.BOT_LEFT;
			break;
		case CollidesOn.BOT_LEFT:
			movedCollidingFrom = CollidesOn.TOP_RIGHT;
			break;
		case CollidesOn.BOT_RIGHT:
			movedCollidingFrom = CollidesOn.TOP_LEFT;
			break;
		default:
			break;
		}

		return afterCollisionDetection(adjustedVector(desiredMoveLocation, randomDirection));

	}

	private int collidesOnMoveLocation(Boolean[] collidesOnDirection, Vector2f desiredMoveLocation) {
		for (int i = 0; i < 9; i++) {
			if (collidesOnDirection[i])
				continue;
			Vector2f adjustedVector = adjustedVector(desiredMoveLocation, i);

			if (moveLocation != null) {
				if (Outliner.getOutline(moveLocation, getOutline().getBoundingCircleRadius())
						.contains(adjustedVector.x, adjustedVector.y)) {
					return i;
				}
			}

			if (getOutline().contains(adjustedVector.x, adjustedVector.y)) {
				return i;
			}
		}
		return -1;
	}

	private Vector2f adjustedVector(Vector2f vta, int adjustTo) {
		switch (adjustTo) {
		case CollidesOn.CENTER:
			return vta;

		case CollidesOn.TOP:
			return new Vector2f(vta.getX(), vta.getY() - (getOutline().getBoundingCircleRadius() * 2) - 2);

		case CollidesOn.BOTTOM:
			return new Vector2f(vta.getX(), vta.getY() + (getOutline().getBoundingCircleRadius() * 2) + 2);

		case CollidesOn.LEFT:
			return new Vector2f(vta.getX() - (getOutline().getBoundingCircleRadius() * 2) - 2, vta.getY());

		case CollidesOn.RIGHT:
			return new Vector2f(vta.getX() + (getOutline().getBoundingCircleRadius() * 2) + 2, vta.getY());

		case CollidesOn.TOP_LEFT:
			return new Vector2f(vta.getX() - (getOutline().getBoundingCircleRadius() * 2) - 2,
					vta.getY() - (getOutline().getBoundingCircleRadius() * 2) - 2);

		case CollidesOn.TOP_RIGHT:
			return new Vector2f(vta.getX() + (getOutline().getBoundingCircleRadius() * 2) + 2,
					vta.getY() - (getOutline().getBoundingCircleRadius() * 2) - 2);

		case CollidesOn.BOT_LEFT:
			return new Vector2f(vta.getX() - (getOutline().getBoundingCircleRadius() * 2) - 2,
					vta.getY() + (getOutline().getBoundingCircleRadius() * 2) + 2);

		case CollidesOn.BOT_RIGHT:
			return new Vector2f(vta.getX() + (getOutline().getBoundingCircleRadius() * 2) + 2,
					vta.getY() + (getOutline().getBoundingCircleRadius() * 2) + 2);
		}

		System.out.println("Error adjusting!, cannot adjust to direction: " + adjustTo);
		return null;
	}

	public static class CollidesOn {
		public static final int CENTER = 0;
		public static final int TOP = 1;
		public static final int BOTTOM = 2;
		public static final int LEFT = 3;
		public static final int RIGHT = 4;
		public static final int TOP_LEFT = 5;
		public static final int TOP_RIGHT = 6;
		public static final int BOT_LEFT = 7;
		public static final int BOT_RIGHT = 8;
	}
}
